import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class rateReviewService {
    constructor(private http: Http, private config: webService) {
    }

    submitRateReview(countryCode,obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseReviewApi.replace('{0}',countryCode) + this.config.submitApplicationReviewUrl
        return this.http.post(url, obj, {headers:this.config.headers})
            .map(response => response.json())
    }
    getRateOptions(countryCode): Observable<any> {
        const url = this.config.baseIp + this.config.baseReviewApi.replace('{0}',countryCode) + this.config.getApplicationOptionsUrl
        return this.http.get(url,{headers:this.config.headers})
            .map(response => response.json())
    }
    sendContactUsMsg(countryCode,obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseReviewApi.replace('{0}',countryCode) + this.config.contactUsMsgUrl
        return this.http.post(url, obj, {headers:this.config.headers})
            .map(response => response.json())
    }
    getComplaintOtions(countryCode): Observable<any> {
        const url = this.config.baseIp + this.config.baseReviewApi.replace('{0}',countryCode) + this.config.getComplaintOptionsUrl
        return this.http.get(url,{headers:this.config.headers})
            .map(response => response.json())
    }
    sendComplain(countryCode,obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseReviewApi.replace('{0}',countryCode) + this.config.sendComplaintUrl
        return this.http.post(url, obj, {headers:this.config.headers})
            .map(response => response.json())
    }
}