import { Component, OnInit, ViewChild, ElementRef, NgModule, NgZone, Inject } from '@angular/core';
import { FormsModule,FormControl } from '@angular/forms';
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import { } from '@types/googlemaps';
import * as $ from 'jquery';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { locationsService } from '../../services/location/locationService'
import { addressService } from '../../services/customer/addressService'
import { accountService } from '../../services/customer/customerAccount'
import { DaliogService } from '../../services/confirmMsg';

declare var google: any;

@Component({
    moduleId: module.id,
    selector: 'app-googleMapAddressPopup',
    templateUrl: 'googleMapAddressPopup.html'
})

export class addressesMapPopupComponent implements OnInit {
constructor(private locationsS: locationsService, private addressS: addressService, private accountS: accountService,
    private customDialogService: DaliogService, private dailog: MatDialog,private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone, public dialogRef: MatDialogRef<addressesMapPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
}
ngOnInit() {
    //set google maps defaults
    this.zoom = 10;
    this.latitude = 29.364074;
    this.longitude = 47.967134;
    
    //set current position
    this.setCurrentPosition();

    }

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  public currentAddress: string;
  private geoCoder;
  

  markerDragEnd($event) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.geoCoder.geocode({'location': {lat: this.latitude, lng: this.longitude }}, (results, status) => {

      if (status === 'OK') {
        if (results[0]) {
          this.currentAddress = results[0].formatted_address;
          // this.searchElementRef.nativeElement.value = results[0].formatted_address);
          // console.log(this.searchElementRef.nativeElement.value);
          // infowindow.setContent(results[0].formatted_address);
          
        } else {
          window.alert('No results found');
        }

      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
  
 setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

}