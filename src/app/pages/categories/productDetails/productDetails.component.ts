import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { categoriesService } from '../../../services/inventory/categoriesService'


@Component({
    moduleId: module.id,
    selector: 'app-productDetails',
    templateUrl: 'productDetails.component.html'
})

export class productDetailsComponent implements OnInit {

    constructor(private categoriesS: categoriesService,private route: ActivatedRoute) {
    }

    id:any = this.route.snapshot.params['id']
    productDetailes:any = {};
    pieceNum: any = {};
    countryCode = localStorage.getItem('countryCode');
    
    images:any = [
        '../assets/img/products/p1.png',
        '../assets/img/products/p2.png',
        '../assets/img/products/p3.png',
        '../assets/img/products/p4.png',
    ]

    ngOnInit() {
        this.categoriesS.getProductById(this.countryCode,this.id).subscribe(itemDetails => {
            this.productDetailes = itemDetails.data;
        }, error => {
            console.log('error')
        })
    }


}


