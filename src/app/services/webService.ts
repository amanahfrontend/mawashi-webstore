import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';


@Injectable()

export class webService {

    contentHeader = JSON.parse(localStorage.getItem('mwashi_currentAuthorzation'))
    headers = new Headers({'Authorization':'bearer'+' ' + this.contentHeader});
    checkoutHeader = new Headers({'Authorization':'bearer'+' ' + this.contentHeader, 'Platform' : 'MobileAndroid'});

    baseIp = 'http://159.89.229.238:9000/';
    oldBaseIp= 'http://159.89.229.238:9000/';
    baseServicesInventory = 'inventoryApi/{0}/v1/';
    baseServicesCustomer = 'customerapi/v1/';
    baseServicesidentity = 'identityapi/v1/';
    baseServicesOrders = 'orderApi/{0}/v1/';
    baseServiceLocation = 'locationapi/v1/';
    baseServicePaymentApi = 'paymentApi/{0}/v1/';
    baseServiceCart = 'cartApi/{0}/v1/';
    baseContentapi='contentapi/{0}/';
    baseReviewApi = 'reviewApi/{0}/v1/';
    baseSettingApi= 'settingApi/{0}/v1/';
    baseContentAdsapi='contentapi/v1/';


    ////// inventory //////
    getAllCategories = 'LKP_Category/GetAllActive'
    getCategorieById = 'LKP_Item/GetActiveByCategoryId/web/{1}'
    getItemDetailsById = 'LKP_Item/GetItemDetailsById/{1}'
    GetDeliveryOptionsByCategoryConfigIdUrl = 'CategoryDeliveryOptions/GetDeliveryOptions/{1}' 

    ////// location /////////
        //Makani//
    getMakaniInfoFromCoord = 'makani/GetMakaniInfoFromCoord?lat={0}&lng={1}'
    getMakaniInfoFromNumber = 'makani/GetMakaniInfoFromNumber?makaniNumber={0}'

        //paci//
    getLocationByPaci = 'paci/GetLocationByPaci?paciNumber={0}'
    getAllGovernorates = 'paci/GetAllGovernorates'
    getAreas = 'paci/GetAreas?govId={0}'
    getBlocks = 'paci/GetBlocks?areaId={0}'
    getStreets = 'paci/GetStreets?govId={0}&areaId={1}&blockName={2}'

    ///////customer////////
    postAdressUrl= 'location/addaddress'
    editAddressUrl= 'location/editaddress'
    getAddressById= 'location/Get/{0}'
    getCustomerLocationByIdUrl = 'location/GetByCustomerId?customerId={0}&countryId={1}'
    registerNewCustomer = 'customer/register'
    UpdateCustomerUser = 'customer/UpdateCustomerUser'
    loginUser = 'customer/login'
    getCustomerById = 'customer/GetById?id={0}'
    isUsernameExist = 'user/IsUsernameExist?username={0}'
    isEmailExist = 'user/IsEmailExist?email={0}'
    generateForgetPasswordToken = 'user/GenerateForgetPasswordToken?username={0}'
    changePassword = 'user/ChangePassword'
    forgetPasswordUrl = 'user/forgetPassword'
    getPhoneVerficationToken ='user/GetPhoneVerficationToken'
    confirmPhone = 'user/ConfirmPhone'
    getUserById = 'customer/GetById?id={0}'
    getAllCountriesUrl = 'country/getAll'

    ///////orders////////
    getAllOrdersUrl = 'Order/GetAll'
    postOrderCheckoutUrl = 'Order/checkout'
    getOrderByCustomerUrl = 'Order/GetOrdersByCustomer/{1}'
    getOrderDeliveryFeesUrl= 'DeliveryFees/GetFees'
    getLocations_DatesUrl = 'CategoryLocation/GetLocationsDates'
    getPaymentStatusUrl = 'order/GetPaymentStatus/{1}'
    getOrderCount = 'Order/GetOrderCountByCustomer/{1}'
    getOrderByIdUrl = 'Order/GetDetailedOrder/{1}'
    getCurenctUrl='LKP_Currency/CurrentCurrency'

    ////////payment/////////
    getPaymentMethodeUrl= 'PaymentMethod/GetByCountryDelivery?countryId={1}&deliveryType={2}'

    ////////cart/////////
    submitCartUrl = 'Cart/Submit'
    getCurrentCartUrl = 'Cart/GetCurrentCart/{1}'
    getCartByIdUrl = 'Cart/GetCartWithItems/{1}'
    checkPromoCodeValid ='Cart/IsPromoCodeValid'

    ///////terms//////////
    getTermsTemplateUrl = 'terms.html'

    //////rate////////
    submitApplicationReviewUrl = 'Review/SubmitApplicationReview'
    getApplicationOptionsUrl = 'RatingScale/GetApplicationOptions'


    ////////setting///////
    getsocialmediaUrl = 'Setting/GetSetting/social_network'
    getPhonesUrl = 'Setting/GetSetting/HOTLINE'

    ////////contactUs&Complaint////////
    contactUsMsgUrl= 'Review/ContactUs'

    ///////complaint/////////
    sendComplaintUrl= 'Review/SubmitComplainReview'
    getComplaintOptionsUrl='RatingScale/GetComplainOptions'

    /////////ads//////////
    getAllAdsUrl= 'ad/GetActive?country={0}'
}