import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priceFilter'
})
export class PriceFilterPipe implements PipeTransform {

  transform(list, minPrice: number | undefined, maxPrice:number | undefined) {
    // ES6 array destructuring
    let filterList = list;
    if (minPrice) {
      filterList = filterList.filter(item => {
        return item.price >= +minPrice;
      });
    } 
    
    if (maxPrice) {
      filterList = filterList.filter(item => {
        return item.price <= +maxPrice;
      });
    }
    return  filterList;
  }

}