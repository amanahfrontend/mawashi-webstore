import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { settingService } from '../../services/setting/settingService';
import { rateReviewService } from '../../services/rateReview/rateReviewService'
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';


@Component({
    moduleId: module.id,
    selector: 'app-contacts',
    templateUrl: 'contacts.component.html'
})

export class contactsComponent implements OnInit {

    constructor(private settingS: settingService, private rateReviewS: rateReviewService, private customDialogService: DaliogService,
        private translate: TranslateService) {
    }

    public loading = false;
    userId: any = JSON.parse(localStorage.getItem('userId'));
    countryCode = localStorage.getItem('countryCode');
    lang = localStorage.getItem('lang');
    socialMedia: any = [];
    msgObj: any = {};
    complintObj: any = {};
    CompTypeId: string = '';
    complaintOPtions: any = [];
    lat: number = 29.364074;
    lng: number = 47.967134;
    zoom: number = 2;
    markers: any = [
        {
            latitude: -31.950000,
            longitude: 115.843056,
            name: 'Australia',
            markerUrl: "/assets/img/map/asFlag.png"
        },
        {
            latitude: 29.364074,
            longitude: 47.967134,
            name: 'Al Kuwait',
            markerUrl: "/assets/img/map/kwFlag.png"
        },
        {
            latitude: 25.045781,
            longitude: 55.431886,
            name: 'United Arab Emirates',
            markerUrl: "/assets/img/map/euaFlag.png"
        }
    ]

    ngOnInit() {
        this.getSocialMedia();
        this.getCopmlaintOptions()
    }

    getSocialMedia() {
        this.loading = true;
        this.settingS.getSocialMedia(this.countryCode).subscribe(socilaResult => {
            this.socialMedia = socilaResult.data;
            this.loading = false;
        }, error => {
            console.error('error');
        })
    }

    sendCOntactUsMsg() {
        if (localStorage.getItem('mwashi_currentAuthorzation')) {
            this.msgObj.FK_Reviewer_Id = this.userId
        }
        this.rateReviewS.sendContactUsMsg(this.countryCode, this.msgObj).subscribe(result => {
            this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('contMsgSucs'), false);
            this.msgObj = {};
        }, error => {
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWron'), false)
            console.error('error');
        })
    }

    getCopmlaintOptions() {
        this.rateReviewS.getComplaintOtions(this.countryCode).subscribe(optionsResult => {
            this.complaintOPtions = optionsResult.data;
        }, error => {
            console.log('error')
        })
    }

    sendComplaint() {
        this.complintObj.FK_Reviewer_Id = this.userId;
        this.complintObj.FK_LKP_Rating_Scale_Id = this.CompTypeId;

        this.rateReviewS.sendComplain(this.countryCode, this.complintObj).subscribe(result => {
            this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('complaintSucss'), false);
            this.complintObj = {};
        }, error => {
            console.log('error')
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWrong'), false);
        })
    }

    
}


