import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { accountService } from '../../services/customer/customerAccount';
import { addressService } from '../../services/customer/addressService';
import { ordersService } from '../../services/customer/ordersService';
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../services/shearedService';


@Component({
    moduleId: module.id,
    selector: 'app-commonProfile',
    templateUrl: 'commonProfile.component.html'
})

export class commonProfileComponent implements OnInit {

    constructor(private accountS:accountService, private addressS:addressService,private router: Router,  private translate: TranslateService,
        private customDialogService: DaliogService, private ordersS:ordersService, private sheardS:shearedService) {
    }

    @ViewChild('fileInput') fileInput: ElementRef;
    public loading = false;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    currentUser: any = {}
    currentCustomer: any = {}
    country = localStorage.getItem('country')
    ordersCount: number;
    countryCode = localStorage.getItem('countryCode');
    updteUserObj: any = {};


    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
              if (res) {
                this.router.navigate(['/login'])
              }
            })
          }
          
        this.getCurrentUser();
        this.getCurrentUserAddresses();
        this.getOrderCount();
    }

    getCurrentUser(){
        this.loading = true;
        this.accountS.getUserById(this.userId).subscribe(result =>{
            this.currentUser = result.data;
        },error =>{
            this.loading = false;
            console.log('error')
        })
    }

    getCurrentUserAddresses(){
        this.addressS.getCustomerLocationsById(this.userId,this.country).subscribe(resultCust =>{
            this.currentCustomer = resultCust;
            this.loading = false;
        },error => {
            this.loading = false;
            console.log('error')
        })
    }

    getOrderCount(){
        this.ordersS.getOrderCount(this.countryCode,this.userId).subscribe(result =>{
            this.ordersCount = result.data
        },error => {
            console.log('error')
        })
    }

    logOut() {
        localStorage.removeItem('mwashi_currentAuthorzation');
        localStorage.removeItem('userName');
        localStorage.removeItem('userId')
        localStorage.removeItem('acceptMulti');
        localStorage.removeItem('cartItems');
        localStorage.removeItem('cartItemsId')
        localStorage.removeItem('cartQuantityCount');
        localStorage.removeItem('itemId');
        localStorage.removeItem('optionId');
        localStorage.removeItem('configId');
        localStorage.removeItem('resultConfigId');
        localStorage.removeItem('resultOption');
        localStorage.removeItem('order#');
        localStorage.removeItem('phone');
        localStorage.removeItem('registerUserName')
        localStorage.removeItem("customerActiviation")
        this.sheardS.cartCount = 0;
    
        this.router.navigate(['/categories'])
        location.reload();
      }

}


