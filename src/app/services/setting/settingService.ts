import { Injectable,Output,EventEmitter } from '@angular/core';
import { Http, Headers} from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';


@Injectable()

export class settingService {
    constructor(private http: Http, private config: webService) {
    }

    getSocialMedia(countryCode): Observable<any> {
        const url = this.config.baseIp + this.config.baseSettingApi.replace('{0}',countryCode) + this.config.getsocialmediaUrl
        return this.http.get(url)
            .map(response => response.json())
    }
    getPhones(countryCode): Observable<any> {
        const url = this.config.baseIp + this.config.baseSettingApi.replace('{0}',countryCode) + this.config.getPhonesUrl
        return this.http.get(url)
            .map(response => response.json())
    }
 
}