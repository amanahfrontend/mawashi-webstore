import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy ,NgSwitch} from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';

import { NouisliderModule } from 'ng2-nouislider';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AgmCoreModule } from '@agm/core';
import { OwlModule } from 'ngx-owl-carousel';
import { MatDialogModule, MatButtonModule,MatExpansionModule,MatRadioModule,MatDatepickerModule,MatInputModule,
  MatNativeDateModule,MatCheckboxModule,MatSelectModule,MatPaginatorModule,MatCardModule,MatTabsModule} from '@angular/material';
import { LoadingModule,ANIMATION_TYPES } from 'ngx-loading';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PasswordStrengthBarModule } from 'ng2-password-strength-bar';

import {SlideshowModule} from 'ng-simple-slideshow';
import { GalleryModule } from  '@ngx-gallery/core';
import { LightboxModule } from 'angular2-lightbox';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { NgxGalleryModule } from 'ngx-gallery';

import { AppComponent } from './app.component';
import { routing } from './routing';
import { homeComponent } from './pages/home/home.component';
import { categoriesComponent } from './pages/categories/categories.component';
import { liveStockComponent } from './pages/categories/liveStock/liveStock.component';
import { productDetailsComponent } from './pages/categories/productDetails/productDetails.component';
import { shoppingCartComponent } from './pages/shoppingCart/shoppingCart.component';
import { checkOutAddressComponent } from './pages/checkOut/checkOutAddress/checkOutAddress.component';
import { checkOutDeliveryComponent } from './pages/checkOut/checkOutDelivery/checkOutDelivery.component';
import { checkOutPaymentComponent } from './pages/checkOut/checkOutPayment/checkOutPayment.component';
import { checkOutOrderReviewComponent } from './pages/checkOut/checkOutOrderReview/checkOutOrderReview.component';
import { checkOutConfirmComponent } from './pages/checkOut/checkOutConfirm/checkOutConfirm.component';
import { customerLoginComponent } from './pages/customerLogin/customerLogin.component';
import { allOrdersComponent } from './pages/orders/allOrders.component';
import { orderDetailsComponent } from './pages/orders/orderDetails/orderDetails.component';
import { profileComponent } from './pages/profile/profile.component';
import { addressesComponent} from './pages/addresses/addresses.component';
import { contactsComponent} from './pages/contacts/contacts.component';
import { productPopupComponent} from './pages/categories/productPopup/productPopup.component';
import { ConfirmDialogComponent } from './pages/confirmMsg/confirmMsg';
import { editAddressComponent} from './pages/addresses/editAddress/editAddressPopup';
import { changePasswordComponent} from './pages/profile/changePassWord/changePassWordComponent';
import { addressesMapPopupComponent} from './pages/googleMapAddressPopup/googleMapAddressPopup';
import { customerRegisterComponent } from './pages/customerRegister/customerRegister.component';
import { orderSummaryComponent } from './pages/orderSummary/orderSummary.component';
import { aboutComponent } from './pages/about/about.component';
import { termsComponent } from './pages/terms/terms.component';
import { corpGovernanceComponent } from './pages/corpGovernance/corpGovernance.component';
import { financeComponent } from './pages/finance/finance.component';
import { marineFleetComponent } from './pages/marineFleet/marineFleet.component';
import { operationComponent } from './pages/operation/operation.component';
import { marketingComponent } from './pages/marketing/marketing.component';
import { hrComponent } from './pages/hr/hr.component';
import { QAComponent } from './pages/QA/QA.component';
import { forgetPssComponent } from './pages/profile/forgetPass/forgetPss.component';
import { custActivationComponent } from './pages/customerActiviation/customerActiviation.component';
import { complaintComponent } from './pages/complaint/complaint.component'
import { commonProfileComponent } from './pages/commonProfile/commonProfile.component';
import { adsComponent } from './pages/ads/ads.component'


///// services /////
import { webService } from './services/webService'
import { DaliogService } from './services/confirmMsg'
import { categoriesService } from './services/inventory/categoriesService'
import { locationsService } from './services/location/locationService'
import { addressService } from './services/customer/addressService'
import { accountService } from './services/customer/customerAccount'
import { ordersService } from './services/customer/ordersService'
import { paymentService } from './services/payment/paymentService'
import { checkOutService } from './services/checkout/checkoutService'
import { cartService } from './services/cart/cartService'
import { shearedService } from './services/shearedService'
import { AngularInterceptor } from './services/Interceptor'
import { rateReviewService } from './services/rateReview/rateReviewService'
import { settingService } from './services/setting/settingService'


///pipes///
import { SafePipe } from './pipes/safePipe'
import { EvenOddPipe } from './pipes/evenOddPipe'

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,homeComponent,categoriesComponent,liveStockComponent,productDetailsComponent,shoppingCartComponent,
    checkOutAddressComponent,checkOutDeliveryComponent,checkOutPaymentComponent,checkOutOrderReviewComponent,checkOutConfirmComponent,
    customerLoginComponent,allOrdersComponent,orderDetailsComponent,profileComponent,addressesComponent,contactsComponent,
    productPopupComponent,ConfirmDialogComponent,SafePipe,editAddressComponent,changePasswordComponent,addressesMapPopupComponent,
    customerRegisterComponent,orderSummaryComponent,aboutComponent,termsComponent,corpGovernanceComponent,financeComponent,
    marineFleetComponent,operationComponent,marketingComponent,hrComponent,QAComponent,forgetPssComponent,custActivationComponent,
    EvenOddPipe,complaintComponent,commonProfileComponent,adsComponent

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NouisliderModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot(
      {
      libraries: ["places"],
      apiKey:'AIzaSyC-QiLxlVtVphjtzyYeDsbYFBT-o3_S_vI'
    }),
    OwlModule,
    MatDialogModule,
    MatButtonModule,
    MatExpansionModule,
    MatRadioModule,
    MatDatepickerModule,
    MatInputModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatSelectModule,
    MatPaginatorModule,
    MatCardModule,
    MatTabsModule,
    PasswordStrengthBarModule,
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.wanderingCubes,
      backdropBackgroundColour: 'rgba(0,0,0,0.1)', 
      backdropBorderRadius: '4px',
      primaryColour: '#ae2322', 
      secondaryColour: '#ae2322', 
      tertiaryColour: '#ae2322'
  }),
  NgbModule.forRoot(),
  SlideshowModule,
  GalleryModule.forRoot(),
  LightboxModule,
  NgxImageGalleryModule,
  NgxGalleryModule
  ],
  entryComponents: [productPopupComponent,ConfirmDialogComponent,editAddressComponent,addressesMapPopupComponent,termsComponent,adsComponent],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }, { provide: HTTP_INTERCEPTORS, useClass: 
    AngularInterceptor, multi: true },webService,DaliogService,categoriesService,locationsService,rateReviewService,
      addressService,accountService,ordersService,paymentService,checkOutService,cartService,shearedService,settingService],
  bootstrap: [AppComponent]
})


export class AppModule { }
