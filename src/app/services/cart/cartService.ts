import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class cartService {
    constructor(private http: Http, private config: webService) {
    }

    submitCart(countryCode,obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseServiceCart.replace('{0}', countryCode) + this.config.submitCartUrl
        return this.http.post(url, obj, { headers: this.config.headers })
            .map(response => response.json())
    }
    getCurrentCart(countryCode,userId, token): Observable<any> {
        const url = this.config.baseIp + this.config.baseServiceCart.replace('{0}', countryCode) + this.config.getCurrentCartUrl.replace('{1}', userId)
        return this.http.get(url, { headers: new Headers({ 'Authorization': 'bearer' + ' ' + token }) })
            .map(response => response.json())
    }
    getCartOverview(countryCode,userId): Observable<any> {
        const url = this.config.baseIp + this.config.baseServiceCart.replace('{0}', countryCode) + this.config.getCurrentCartUrl.replace('{1}', userId)
        return this.http.get(url, { headers: this.config.headers})
            .map(response => response.json())
    }
    getCartById(countryCode,carId): Observable<any> {
        const url = this.config.baseIp + this.config.baseServiceCart.replace('{0}', countryCode) + this.config.getCartByIdUrl.replace('{1}', carId)
        return this.http.get(url, { headers: this.config.headers})
            .map(response => response.json())
    }
    checkPromoCodeValid(countryCode,obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseServiceCart.replace('{0}', countryCode) + this.config.checkPromoCodeValid
        return this.http.post(url, obj, { headers: this.config.headers })
            .map(response => response.json())
    }
}