import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';
import { DaliogService } from './services/confirmMsg';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from "rxjs/Subscription";
import { Observable } from "rxjs/Observable";
import { shearedService } from './services/shearedService';
import { addressService } from './services/customer/addressService'
import { categoriesService } from './services/inventory/categoriesService'
import { settingService } from './services/setting/settingService'
import { all } from 'q';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {

  constructor(private customDialogService: DaliogService, private translate: TranslateService, private router: Router,
    private shearedS: shearedService, private addressS: addressService, private categoriesS: categoriesService,
    private settingS: settingService) {
    this.translate = translate;
    translate.setDefaultLang('en');
  }

  public loading = false;
  isloged: boolean = false;
  notloged: boolean = true;
  ischeckedKW: boolean = false;
  ischeckedUAE: boolean = false;
  itemCount: number;
  countryName: string;
  private cartSubscription: Subscription;
  country: string = '';
  countries: any = []
  selectedCountry: any = "";
  selectedCountryName = localStorage.getItem('countryName');
  countryCode: string = "";
  activeCheckOut: boolean = false;
  // tempList: any = this.shearedS.selectedItems;
  tempList: any;
  cartQuantityCount: any = 0;
  sum: number = 0;
  totalPrice: number = 0;
  isAr: boolean = false;
  categories: any = [];
  lang = localStorage.getItem('lang')
  itemId: any;
  phones: any = [];
  socialMedia: any = [];
  cartItems = localStorage.getItem('cartItems')


  ngOnInit() {
    this.initializeLocalStorage();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });

    this.getAllCategories();
    this.getAllCountries();
    this.switchLanguage(localStorage.getItem('lang'), false)
    this.switchCountry(localStorage.getItem('country'), false)
    this.getSocialMedia();
    this.getPhones();
    this.getCatItems();

  }

  initializeLocalStorage() {
    if (!localStorage.getItem('mwashi_currentAuthorzation')) {
      this.isloged = false;
      this.notloged = true;
    }
    else {
      this.isloged = true;
      this.notloged = false;
    }

    this.cartQuantityCount = JSON.parse(localStorage.getItem('cartQuantityCount'))


    if (!this.cartQuantityCount) {
      this.shearedS.cartCount = 0;
    }
    else {
      this.shearedS.cartCount = this.cartQuantityCount;
    }

    if (localStorage.getItem('cartItemsId')) {
      this.activeCheckOut = true;
    }
    else {
      this.activeCheckOut = false;
    }

    if (localStorage.getItem('country')) {
      this.selectedCountry = localStorage.getItem('country');
    }
    else {
      localStorage.setItem('country', '2');
      this.selectedCountry = 2;
    }

    if (localStorage.getItem('countryName')) {
      this.selectedCountryName = localStorage.getItem('countryName');
    }
    else {
      localStorage.setItem('countryName', 'UAE');
      this.selectedCountryName = 'UAE';
    }

    if (localStorage.getItem('countryCode')) {
      this.countryCode = localStorage.getItem('countryCode');
    }
    else {
      localStorage.setItem('countryCode', 'uae');
      this.countryCode = "uae";
    }
  }


  getAllCountries() {
    this.addressS.getAllCountries().subscribe(allCountries => {
      this.countries = allCountries;
    }, error => {
      if (error.status == 'failed') {
        alert('connection error')
      }
      console.error('error');
    })
  }
  getAllCategories() {
    this.loading = true;
    this.categoriesS.getAllCategories(this.countryCode).subscribe(allCat => {
      this.categories = allCat.data;
      this.loading = false;
    }, error => { console.log('error') })
  }
  switchLanguage(lang: string, isReload) {
    if (lang == 'ar') {
      this.isAr = true;
      localStorage.setItem('lang', 'ar');
      this.translate.use(lang);
      $('#theme-stylesheet').after('<link id="styleAr" rel="stylesheet" href="./assets/css/styleAr.css" type="text/css" />');
    }
    else {
      localStorage.setItem('lang', 'en');
      this.translate.use(lang);
      $('#styleAr').remove()
      this.isAr = false;
    }
    if (isReload) {
      this.loading = true;
      location.reload();
    }
  }

  switchCountry(country, isChange) {
    this.selectedCountry = country;
    if (country == 1) {
      this.countryName = "Kuwait";
      localStorage.setItem('country', '1');
      localStorage.setItem('countryCode', 'kw');
    }
    else if (country == 2) {
      this.countryName = "UAE";
      localStorage.setItem('country', '2');
      localStorage.setItem('countryCode', 'uae');
    }
    if (isChange) {
      location.reload();
    }
  }

  switchCountry2(countryId, countryName, isChange) {
    if (countryId) {
      this.selectedCountry = countryId;
      this.selectedCountryName = countryName;
      localStorage.setItem('country', this.selectedCountry);
      localStorage.setItem('countryName', this.selectedCountryName);
      // localStorage.setItem('countryCode','kw');
    }
    if (isChange) {
      location.reload();
    }

  }

  removeItem(item) {
    let decreaseValue = 0;
    for (let i = 0; i < this.tempList.length; i++) {
      if (this.tempList[i].item.id == item.item.id) {
        decreaseValue = this.tempList[i].quantity.value;
        this.tempList.splice(i, 1);
      }
    }
    let cartQuantityCount = parseInt(localStorage.getItem('cartQuantityCount')) - decreaseValue;
    localStorage.setItem('cartItems', JSON.stringify(this.tempList));
    localStorage.setItem('cartQuantityCount', JSON.stringify(cartQuantityCount));
    this.shearedS.cartCount = cartQuantityCount;
  }

  getCatItems() {
    if (localStorage.getItem('cartItems')) {
      this.tempList = JSON.parse(localStorage.getItem('cartItems'))
      for (let i = 0; i < this.tempList.length; i++) {
        if (this.tempList[i].quantity) {
          this.totalPrice = this.tempList[i].quantity.value * this.tempList[i].item.finalPrice;
        }
      }
    }
  }

  viewCategory(catId) {

    if (catId.config) {
      localStorage.setItem('resultConfigId', catId.config.id)
      localStorage.setItem('resultOption', JSON.stringify(catId.config.options))
      localStorage.setItem('acceptMulti', catId.config.acceptMulti)
    }
    this.tempList = JSON.parse(localStorage.getItem('cartItems'))
    if (this.tempList) {
      if (this.tempList.length != 0) {
        localStorage.setItem('acceptMultiStorage', this.tempList[0].acceptMulti)

        let acceptMultiItem = JSON.parse(localStorage.getItem('acceptMulti'));
        let acceptMultiStorage = JSON.parse(localStorage.getItem('acceptMultiStorage'));
        console.log(typeof acceptMultiItem)
        console.log(typeof acceptMultiStorage)
        let acceptMulti = JSON.parse(localStorage.getItem('acceptMulti'))

        // for (let i = 0; i < this.tempList.length; i++) {
        if (this.tempList[0].item.fK_Category_Id == catId.id) {
          this.router.navigate(['categories/' + catId.id]);
        }
        else if (acceptMultiStorage == true && acceptMultiItem == true) {
          this.router.navigate(['categories/' + catId.id]);
        }
        else if (acceptMultiStorage == false && acceptMultiItem == false) {
          this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
        }
        else if (acceptMultiStorage !== acceptMultiItem) {
          this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
        }

        // }
      }
      else {
        this.router.navigate(['/categories/', catId.id])
      }
    }

    else {
      this.router.navigate(['/categories/', catId.id]);
    }
  }

  logOut() {
    localStorage.removeItem('mwashi_currentAuthorzation');
    localStorage.removeItem('userName');
    localStorage.removeItem('userId')
    localStorage.removeItem('acceptMulti');
    localStorage.removeItem('cartItems');
    localStorage.removeItem('cartItemsId')
    localStorage.removeItem('cartQuantityCount');
    localStorage.removeItem('itemId');
    localStorage.removeItem('optionId');
    localStorage.removeItem('configId');
    localStorage.removeItem('resultConfigId');
    localStorage.removeItem('resultOption');
    localStorage.removeItem('order#');
    localStorage.removeItem('phone');
    localStorage.removeItem('registerUserName')
    localStorage.removeItem("customerActiviation")
    this.shearedS.cartCount = 0;

    this.router.navigate(['/categories'])
    location.reload();
  }

  goHome() {
    this.router.navigate(['home']);
    this.loadScripts();
  }

  loadScripts() {
    if (document.getElementById("script2")) {
      document.getElementById("script2").remove();
    }
    var script2 = document.createElement("script");
    script2.setAttribute("id", "script2");
    script2.setAttribute("src", "./assets/js/front.js");
    document.head.appendChild(script2);
  }

  getPhones() {
    this.loading = true;
    this.settingS.getPhones(this.countryCode).subscribe(phoneResult => {
      this.phones = phoneResult.data;
      this.loading = false;
    }, error => {
      console.error('error');
    })
  }

  getSocialMedia() {
    this.loading = true;
    this.settingS.getSocialMedia(this.countryCode).subscribe(socilaResult => {
      this.socialMedia = socilaResult.data;
      this.loading = false;
    }, error => {
      console.error('error');
    })
  }

}



