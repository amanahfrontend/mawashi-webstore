import { Component, OnInit, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { checkOutService } from '../../../services/checkout/checkoutService'
import { DaliogService } from '../../../services/confirmMsg';
import { DOCUMENT } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import * as $ from 'jquery'
import { cartService } from '../../../services/cart/cartService'
import { ordersService } from '../../../services/customer/ordersService'
import { MatDialog,MatDialogRef } from '@angular/material';
import { termsComponent } from '../../terms/terms.component'
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../../services/shearedService';



@Component({
    moduleId: module.id,
    selector: 'app-checkOutOrderReview',
    templateUrl: 'checkOutOrderReview.component.html'
})

export class checkOutOrderReviewComponent implements OnInit {

    constructor(private checkOutS: checkOutService, private customDialogService: DaliogService, private router: Router,
        private sanitizer: DomSanitizer, private cartS: cartService, private ordersS: ordersService,public dialog: MatDialog,
        private orderS:ordersService,private translate: TranslateService, private sheardS:shearedService) {

    }

    
    public loading = false;
    checkoutObj: any = {};
    customerId: any = JSON.parse(localStorage.getItem('userId'))
    locationId = JSON.parse(localStorage.getItem('locationId'))
    deliveryDate = localStorage.getItem('deliverDate')
    methodId = localStorage.getItem('PaymentTypeCode')
    deliveryNote = localStorage.getItem('deliveryNote')
    countryCode = localStorage.getItem('countryCode')
    deliverTypeId = localStorage.getItem('deliveryTypeId')
    payObj: string = ''
    isPOst: boolean = false;
    isFrame: boolean = true;
    cartItemsId = JSON.parse(localStorage.getItem('cartItemsId'))
    submitedCartObj: any = {};
    userId: any = JSON.parse(localStorage.getItem('userId'))
    total: any;
    submitedOrder: any = {}
    termsDialog: MatDialogRef<termsComponent>
    lang = localStorage.getItem('lang');
    cartResultLength: any;


    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
                if (res) {
                    this.router.navigate(['/login'])
                }
            })
        }

        $('body').on('keydown', 'input', function (e) {
            if (e.which === 32 && e.target.selectionStart === 0) {
                return false;
            }
        });
        $('body').on('keydown', 'textarea', function (e) {
            if (e.which === 32 && e.target.selectionStart === 0) {
                return false;
            }
        });

        this.getCurrentCart();
    }

    stringDate(date) {
        let d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getUTCHours(),
            date.getUTCMinutes(),
            date.getUTCSeconds()));
        return d.toISOString().split('T')[0]
    }

    getCurrentCart() {
        this.cartS.getCartOverview(this.countryCode, this.userId).subscribe(result => {
            console.log(result.data)
            this.submitedCartObj = result.data;
            this.cartResultLength = this.submitedCartObj.cartItems.length;
            if(this.cartResultLength > 1){
                for(let i=0; i< this.submitedCartObj.cartItems.length; i++){
                    this.total += Math.ceil(this.submitedCartObj.price + this.submitedCartObj.vat)
                }
            }
            else{
                this.total = Math.ceil(this.submitedCartObj.price + this.submitedCartObj.vat)
            }
           
        }, error => console.log('error'))
    }

    openTermsDialog() {
        this.termsDialog = this.dialog.open(termsComponent, {
          height: '475px',
          width:'480px'
        });
        this.termsDialog.afterClosed().subscribe(result => {
            this.postOrder();
          console.log(`Dialog result: ${result}`);
        });
      }

      back(){
        this.sheardS.PaymentTypeCode;
        console.log(this.sheardS.PaymentTypeCode)
        this.router.navigate(['/checkOut/checkOutPayment'])
    }

      postOrder() {
        this.checkoutObj.FK_Cart_Id = this.cartItemsId;
        this.checkoutObj.FK_Customer_Id = this.customerId;
        this.checkoutObj.Fk_Location_Id = this.sheardS.locationId;
        this.checkoutObj.FK_DeliveryType_Id = this.sheardS.deliveryTypeId;
        this.checkoutObj.DeliveryDate = this.sheardS.deliveryDate;
        this.checkoutObj.PaymentTypeCode = this.sheardS.PaymentTypeCode;
        this.checkoutObj.deliveryNote = this.sheardS.deliveryNote;
        this.checkoutObj.promoCode = this.sheardS.promoCode;

        console.log(this.checkoutObj);

        this.checkOutS.postCheckOut(this.countryCode, this.checkoutObj).subscribe(result => {
            console.log(this.checkoutObj);
            this.submitedOrder = result.data
            if (!result.data) {
                this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWrongSndOrdr'), false);
            }
            else {
                
                localStorage.setItem('order#',this.submitedOrder.code);

                if (result.data.paymentURL) {
                    this.payObj = result.data.paymentURL;
                    this.isFrame = true;
                    this.sendUrl(result.data.paymentURL)
                }
                else {
                    this.router.navigate(['/checkOut/checkOutConfirm/', true])
                }
                localStorage.removeItem('cartItems')
                localStorage.removeItem('cartQuantityCount')
                localStorage.removeItem('deliverDate')
                localStorage.removeItem('deliveryNote')
                localStorage.removeItem('acceptMulti')
                localStorage.removeItem('PaymentTypeCode')
                localStorage.removeItem('locationId')
                localStorage.removeItem('resultConfigId')
                localStorage.removeItem('resultOption')
                localStorage.removeItem('cartItemsId')
                localStorage.removeItem('configId')
                localStorage.removeItem('deliveryTypeId')
                localStorage.removeItem('itemId')
                localStorage.removeItem('optionId')
                this.sheardS.cartCount = 0
            }

            // window.location.href = result.data.paymentURL;
            // this.router.navigate(['/checkOut/checkOutConfirm'])
            // document.write("<embed src='result.data.paymentURL' width='600' height='500'>")
            // document.write('<iframe width="100%" height="300" src={{result.data.paymentURL | safe}}></iframe>')
            // var windowUrl = window.open( result.data.paymentURL, '_blank', 'location=yes,height=570,width=620,scrollbars=yes,status=yes')
            // alert(windowUrl)

        }, error => {
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWrongSndOrdr'), false);
        })
    }

    source: string = '';

    onLoadFunc(myIframe) {
        // this.source = myIframe.contentWindow.location.href;
            console.log(myIframe)
        this.orderS.getPaymentStatus(this.countryCode, this.submitedOrder.id).subscribe(result => {

            if (result.data) {
                console.log("data");
                if (result.data.isPaymentProcessed) {
                    if (result.data.isPaymentSucceeded == true) {
                        // payment done and successed
                        console.log(result.data.paymentMessage);
                        this.router.navigate(['/checkOut/checkOutConfirm/', true])
                    } else {
                        // payment done and failed
                        console.log(result.data.paymentMessage);
                        this.router.navigate(['/checkOut/checkOutConfirm/', false])
                    }
                }
                // payment is not processed
            }
            console.log('orderId' + this.submitedOrder.id)
            console.log('paymentResult' + result)
        })

    }

    trustedDashboardUrl: any;
    iframeURL: any;

    sendUrl(playerUrl) {
        // this.iframeURL = playerUrl // try it first, if it doesn't work use the sanitized URL
        this.trustedDashboardUrl = this.sanitizer.bypassSecurityTrustResourceUrl(playerUrl);
        this.iframeURL = this.trustedDashboardUrl;
        this.isPOst = true;
        this.isFrame = false;
        // document.write('<iframe width="100%" height="300" src={{this.trustedDashboardUrl | safe}}></iframe>')
    }

    
}


