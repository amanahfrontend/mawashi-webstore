import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { accountService } from '../../services/customer/customerAccount'
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';


@Component({
    moduleId: module.id,
    selector: 'app-customerRegister',
    templateUrl: 'customerRegister.component.html'
})

export class customerRegisterComponent implements OnInit {

    constructor(private accountS: accountService, private customDialogService: DaliogService, private router: Router,
        private translate: TranslateService) {
    }

    loading: boolean = false;

    @ViewChild('fileInput') fileInput: ElementRef;

    country = localStorage.getItem('country')

    registerObj: any = {};
    phoneCode: any;
    myForm:any;

    public barLabel: string = "Password strength:";
    public myColors = ['#DD2C00', '#FF6D00', '#FFD600', '#AEEA00', '#00C853'];


    ngOnInit() {
        if (this.country == '1') {
            this.phoneCode = '00965'
        }
        else {
            this.phoneCode = '00971'
        }

        this.myForm = new FormGroup({
            phoneUae: new FormControl(null, Validators.minLength(9)),
            phoneKw: new FormControl(null, Validators.minLength(8)),
            userName: new FormControl(null, Validators.required),
            firstName: new FormControl(null, Validators.required),
            lastName: new FormControl(null, Validators.required),
            email: new FormControl(null, Validators.required),
            password: new FormControl(null, Validators.required),
            // countryCode: new FormControl(null, Validators.required),
            phoneCode: new FormControl({value: this.phoneCode, disabled: true}, Validators.required),
            picture: new FormControl()
        });
    }

    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.registerObj.picture = reader.result.split(',')[1]
            };
        }
    }

    clearFile() {
        this.fileInput.nativeElement.value = '';
    }

    registerCustomer() {
        this.loading = true;
        this.registerObj.phone1 = this.phoneCode + this.registerObj.phone1;
        this.registerObj.userName = this.registerObj.email;
        this.registerObj.fk_Country_Id = this.country
        this.accountS.register(this.registerObj).subscribe(result => {
            this.loading = false;
            if (result.data != null) {
                this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('rigesrtSucss'), false);
                this.router.navigate(['/customerActiviation'])
                localStorage.setItem('registerUserName', result.data.userName)
                localStorage.setItem('phone', result.data.phone1)
            }
            else {
                this.loading = false;
                this.registerObj.phone1 = ''
                this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWrong'), false);
            }

        }, error => {
            this.loading = false;
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWrong'), false);
        })
    }

    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;
        let inputChar = String.fromCharCode(event.charCode);
    
        if (!pattern.test(inputChar)) {
          // invalid character, prevent input
          event.preventDefault();
        }
    }    
    keyPressNegative(e: any) {
        if (!((e.keyCode > 95 && e.keyCode < 106)
          || (e.keyCode > 47 && e.keyCode < 58)
          || e.keyCode == 8)) {
          return false;
        }
      }
}


