import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { categoriesService } from '../../services/inventory/categoriesService'
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { adsComponent } from '../ads/ads.component';
import { Observable } from 'rxjs';

import { checkOutService } from '../../services/checkout/checkoutService';

import * as $ from 'jquery';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';


@Component({
    moduleId: module.id,
    selector: 'app-home',
    templateUrl: 'home.component.html'
})

export class homeComponent implements OnInit {

    constructor(private categoriesS: categoriesService, private router: Router, private customDialogService: DaliogService,
        private translate: TranslateService, private dailog: MatDialog, private checkOutS: checkOutService) {
    }

    categories: any = [];
    countryCode: string = "";
    cartStorage: any = [];
    lang = localStorage.getItem('lang');
    adsDialog: MatDialogRef<adsComponent>;
    stopCondition: boolean = false;
    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];

    country = localStorage.getItem('country');
    ads: any = [];

    ngOnInit() {
        if (localStorage.getItem('cartItems')) {
            this.cartStorage = JSON.parse(localStorage.getItem('cartItems'));
        }
        this.countryCode = localStorage.getItem('countryCode');
        this.getAllCategories(this.countryCode);

        if (document.getElementById("script2")) {
            document.getElementById("script2").remove();
        }
        var script2 = document.createElement("script");
        script2.setAttribute("id", "script2");
        script2.setAttribute("src", "./assets/js/front.js");
        document.head.appendChild(script2);

        this.getAllAds();

        this.galleryOptions = [
            {
                width: '320px',
                height: '340px',
                imageAnimation: NgxGalleryAnimation.Slide,
                imageAutoPlay: true,
                thumbnails: false,
                imageArrowsAutoHide: true,

            }
        ];

        setTimeout(() => {
            // this.showAds();
            $('#ads').fadeIn().css('visibility', 'visible');
        }, 3000);
    }


    showAds() {
        this.adsDialog = this.dailog.open(adsComponent, {
            width: '340px',
            height: '430px'
        })
        this.adsDialog.updatePosition({ bottom: '30px', right: '30px' })

        this.adsDialog.afterClosed().subscribe((result) => {
            // this.stopCondition = false;
            setInterval(() => {
                this.showAds();
            }, 1000*60*60);
        })
    }

    dialogClose(){
        $('#ads').fadeOut().css('visibility', 'hidden');
    }

    getAllCategories(code) {
        this.categoriesS.getAllCategories(code).subscribe(allCat => {
            this.categories = allCat.data;
        }, error => { console.log('error') })
    }


    viewCategory(catId) {
        if (catId.config) {
            localStorage.setItem('resultConfigId', catId.config.id)
            localStorage.setItem('resultOption', JSON.stringify(catId.config.options))
            localStorage.setItem('acceptMulti', catId.config.acceptMulti)
        }
        if (this.cartStorage) {
            if (this.cartStorage.length > 0) {
                let acceptMulti = JSON.parse(localStorage.getItem('acceptMulti'))
                for (let i = 0; i < this.cartStorage.length; i++) {
                    if (this.cartStorage[i].item.fK_Category_Id == catId.id) {
                        this.router.navigate(['categories/' + catId.id]);
                    }
                    else if ((this.cartStorage[i].acceptMulti === acceptMulti) === true) {
                        this.router.navigate(['categories/' + catId.id]);
                    }
                    else if ((this.cartStorage[i].acceptMulti === acceptMulti) === false) {
                        this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                    }
                    else if (this.cartStorage[i].acceptMulti !== acceptMulti) {
                        this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                    }
                }
            }
            else {
                this.router.navigate(['categories/' + catId.id])
            }
        }

        else {
            if (JSON.parse(localStorage.getItem('acceptMulti')) == false) {
                this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddAnothrItemToCrt'), false);
            }
            this.router.navigate(['categories/' + catId.id]);
        }
    }

    getAllAds() {
        this.checkOutS.getAds(this.country).subscribe(result => {
            if (result.data && result.data.length > 0) {
                result.data.map((ad) => {
                    this.ads.push({
                        medium: ad,
                        big: ad
                    });
                });
            }
        }, error => {
            console.log('error')
        })
    }

}


