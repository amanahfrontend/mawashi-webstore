import { Component, OnInit, Input, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { productPopupComponent } from '../categories/productPopup/productPopup.component'
import { categoriesService } from '../../services/inventory/categoriesService'
import { DaliogService } from '../../services/confirmMsg';
import { cartService } from '../../services/cart/cartService'
import { addressService } from '../../services/customer/addressService'
import { accountService } from '../../services/customer/customerAccount'
import { shearedService } from '../../services/shearedService'
import { ThrowStmt } from '@angular/compiler';
import { TranslateService } from '@ngx-translate/core';



@Component({
    moduleId: module.id,
    selector: 'app-shoppingCart',
    templateUrl: 'shoppingCart.component.html',
})

export class shoppingCartComponent implements OnInit {

    @Input() productPopupC: productPopupComponent;

    constructor(private categoriesS: categoriesService, private shearedS: shearedService, private cartS: cartService, private translate: TranslateService,
        private router: Router, private customDialogService: DaliogService, private addressS: addressService, private accountS: accountService) {
    }

    public loading = false;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    productDetailes: any = {}
    quantityNum: any;
    cartObj: any = { FK_Customer_Id: "", CartItems: [], id: 0 };
    lang = localStorage.getItem('lang');
    // tempList: any = this.shearedS.selectedItems;
    tempList: any = JSON.parse(localStorage.getItem('cartItems'))
    productListId = localStorage.getItem('cartItemsId')
    resultConfigId = JSON.parse(localStorage.getItem('resultConfigId'))
    currentCustomer: any = {}
    tempPassingItem: any = {
        Fk_Item_Id: 0,
        FK_Quantity_Id: 0,
        FK_Cutting_Id: 0,
        FK_CategoryConfig_Id: 0,
        FK_Option_Id: 0
    }
    rowData: any = {
        Fk_Item_Id: 0,
        FK_Quantity_Id: 0,
        FK_Cutting_Id: 0,
        FK_CategoryConfig_Id: 0,
        FK_Option_Id: 0
    }
    passingCartItems: any = [];
    optionId = ''
    categoryConfigId = JSON.parse(localStorage.getItem('categoryConfigId'))
    countryCode = localStorage.getItem('countryCode');
    total: any;
    totalFinalPrice: any;


    ngOnInit() {
        this.getProductDetailsByIdTest();
        if (this.userId) {
            this.getCurrentCartId();
        }
    }


    removeItem(item) {
    let decreaseValue = 0;
    for (let i = 0; i < this.tempList.length; i++) {
      if (this.tempList[i].item.id == item.item.id) {
        decreaseValue = this.tempList[i].quantity.value;
        this.tempList.splice(i, 1);
      }
    }
    let cartQuantityCount = parseInt(localStorage.getItem('cartQuantityCount')) - decreaseValue;
    localStorage.setItem('cartItems', JSON.stringify(this.tempList));
    localStorage.setItem('cartQuantityCount', JSON.stringify(cartQuantityCount));
    this.shearedS.cartCount = cartQuantityCount;
  }

    getProductDetailsByIdTest() {
        console.log(JSON.stringify(this.tempList))
        if (this.tempList) {
            for (let i = 0; i < this.tempList.length; i++) {
                this.categoriesS.getProductById(this.countryCode, this.tempList[i].item.id).subscribe(itemDetails => {
                    this.productDetailes = itemDetails.data;
                }, error => {
                    console.log('error')
                })
                if (!this.tempList[i].quantity.id) {
                    this.quantityNum = 1;
                }
                this.quantityNum = this.tempList[i].quantity.value;
                console.log(this.quantityNum)
                this.total= this.tempList[i].item.finalPrice + this.tempList[i].cutting.price;
                this.totalFinalPrice = this.tempList[i].quantity.value * this.total;
            }
        }
    }

    submitCart() {
        this.loading = true;
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            this.loading = false;
            result.subscribe((res) => {
                if (res) {
                    this.router.navigate(['/login'])
                }
            })
        }
        else {

            if (localStorage.getItem('optionId')) {
                this.optionId = localStorage.getItem('optionId')
            }
            else {
                this.optionId = '0';
            }

            this.cartObj.CartItems = JSON.parse(localStorage.getItem('cartItems'));
            console.log(JSON.stringify(this.cartObj.CartItems))
            this.cartObj.FK_Customer_Id = this.userId;



            if (localStorage.getItem('cartItemsId') == null) {
                this.cartObj.id = 0;
            } else {
                this.cartObj.id = JSON.parse(localStorage.getItem('cartItemsId'));
            }

            console.log("Log ya ", this.cartObj.CartItems)

            this.passingCartItems = [];
            this.cartObj.CartItems.forEach(row => {
                console.log("i'm row");
                console.log(row);
                if (row.cutting.id == undefined || row.cutting.id == null) {
                    row.cutting.id = 0;
                }
                this.passingCartItems.push({
                    Fk_Item_Id: row.item.id,
                    FK_Quantity_Id: row.quantity.id,
                    FK_Cutting_Id: row.cutting.id,
                    FK_CategoryConfig_Id: row.configId,
                    FK_Option_Id: row.optionId
                });

            });
            console.log(this.passingCartItems);

            this.cartObj.CartItems = this.passingCartItems;
            console.log(this.cartObj.CartItems);
            console.log(this.cartObj);

            this.cartS.submitCart(this.countryCode, this.cartObj).subscribe(result => {
                this.loading = false;
                if (result.isSucceeded) {
                    this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('sucesSubmtCart'), false);
                    console.log("Log ya ", this.passingCartItems)
                    localStorage.setItem('configId', this.passingCartItems[0].FK_CategoryConfig_Id);
                    localStorage.setItem('cartItemsId', result.data.id);
                    console.log('submited cart', result.data);
                    this.router.navigate(['/checkOut/checkOutAddress'])
                } else {
                    this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('failSubmtCart'), false);
                }
            }, error => {
                this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('failSubmtCart'), false);
            })
        }
    }

    getCurrentCartId() {
        this.cartS.getCartOverview(this.countryCode, this.userId).subscribe(result => {
            if (result.data) {
                console.log("Cart Live");
                console.log(result.data);
                if (result.data.id) {
                    // localStorage.setItem("cartItems", JSON.stringify(this.cartObj));
                    localStorage.setItem("cartItemsId", result.data.id);
                } else {
                    localStorage.setItem("cartItemsId", '0');
                }
                // location.reload();
                console.log(result.data);
            }
        }, error => {
            console.log("Error");
        })
    }


}


