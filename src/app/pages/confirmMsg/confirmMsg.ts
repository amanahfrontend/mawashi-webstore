import { MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
    selector: 'confirm-dialog',
    templateUrl:'confirmMsg.html'
})
export class ConfirmDialogComponent {

    public title: string;
    public message: string;
    public cancelButton:boolean=true
  

    constructor(public dialogRef: MatDialogRef<ConfirmDialogComponent>) {

    }
}