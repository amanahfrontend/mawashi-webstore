import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { rateReviewService } from '../../../services/rateReview/rateReviewService'
import { DaliogService } from '../../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../../services/shearedService';


@Component({
    moduleId: module.id,
    selector: 'app-checkOutConfirm',
    templateUrl: 'checkOutConfirm.component.html'
})

export class checkOutConfirmComponent implements OnInit {

    constructor(private route: ActivatedRoute, private rateReviewS: rateReviewService,private customDialogService: DaliogService,
        private router: Router,private translate: TranslateService, private sheardS: shearedService) {
    }
    private sub: any;
    isSuccess: boolean = false;
    countryCode = localStorage.getItem('countryCode')
    rateOptionsObj: any = {}
    rateOptions: any = [];
    userId: any = JSON.parse(localStorage.getItem('userId'))
    rateOption:string = '';
    orderNum = localStorage.getItem('order#')


    ngOnInit() {
        this.getRateOptions()

        this.sub = this.route.params.subscribe(params => {
            this.isSuccess = params['isSuccess']; 
        });
        this.sheardS.cartCount = 0
    }

    getCustomerOrder(){
    }

    getRateOptions(){
        this.rateReviewS.getRateOptions(this.countryCode).subscribe(result =>{
            this.rateOptions = result.data;
        },error => {
            console.log('error')
        })
    }
    

    submitRateReview(){
        this.rateOptionsObj.FK_Reviewer_Id = this.userId;
        this.rateOptionsObj.FK_LKP_Rating_Scale_Id = this.rateOption;
        this.rateReviewS.submitRateReview(this.countryCode,this.rateOptionsObj).subscribe(result =>{
            this.customDialogService.alert(this.translate.instant('sucss'), this.translate.instant('tnxForRat'), false);            this.router.navigate(['/home'])
        },error =>{
            console.log('error')
        })
    }

}


