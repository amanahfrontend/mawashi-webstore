import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { accountService } from '../../services/customer/customerAccount';
import { ordersService } from '../../services/customer/ordersService';
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import { rateReviewService } from '../../services/rateReview/rateReviewService';
import { shearedService } from '../../services/shearedService';


@Component({
    moduleId: module.id,
    selector: 'app-complaint',
    templateUrl: 'complaint.component.html'
})

export class complaintComponent implements OnInit {

    constructor(private accountS:accountService, private customDialogService: DaliogService,private router: Router,
        private ordersS:ordersService, private translate: TranslateService, private rateReviewS:rateReviewService,
        private sheardS:shearedService) {
    }

    public loading = false;
    userName: any = JSON.parse(localStorage.getItem('userName'))
    complintObj: any = {};
    ordersCount: number;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    countryCode = localStorage.getItem('countryCode');
    lang= localStorage.getItem('lang')
    complaintOPtions: any = [];
    CompTypeId: string = '';
    currentUser: any = {};

    
    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
              if (res) {
                this.router.navigate(['/login'])
              }
            })
          }
          this.getOrderCount();
        this.getCopmlaintOptions();
        this.getCurrentUser();
    }

    getCurrentUser(){
        this.accountS.getUserById(this.userId).subscribe(result =>{
            this.currentUser = result.data;
        },error =>{
            if(error.status == 401){
            console.log('error')
            }
            console.log('error')
        })
    }

    getCopmlaintOptions(){
        this.rateReviewS.getComplaintOtions(this.countryCode).subscribe(optionsResult =>{
            this.complaintOPtions = optionsResult.data;
        },error =>{
            console.log('error')
        })
    }

    sendComplaint(){
        this.complintObj.FK_Reviewer_Id = this.userId;
        this.complintObj.FK_LKP_Rating_Scale_Id = this.CompTypeId;

        this.rateReviewS.sendComplain(this.countryCode,this.complintObj).subscribe(result =>{
            this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('complaintSucss'), false);
            this.complintObj = {};
        },error =>{
            console.log('error')
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWrong'), false);
        })
    }
    
    getOrderCount(){
        this.ordersS.getOrderCount(this.countryCode,this.userId).subscribe(result =>{
            this.ordersCount = result.data
        },error => {
            console.log('error')
        })
    }

    logOut() {
        localStorage.removeItem('mwashi_currentAuthorzation');
        localStorage.removeItem('userName');
        localStorage.removeItem('userId')
        localStorage.removeItem('acceptMulti');
        localStorage.removeItem('cartItems');
        localStorage.removeItem('cartItemsId')
        localStorage.removeItem('cartQuantityCount');
        localStorage.removeItem('itemId');
        localStorage.removeItem('optionId');
        localStorage.removeItem('configId');
        localStorage.removeItem('resultConfigId');
        localStorage.removeItem('resultOption');
        localStorage.removeItem('order#');
        localStorage.removeItem('phone');
        localStorage.removeItem('registerUserName')
        localStorage.removeItem("customerActiviation")
        this.sheardS.cartCount = 0;
    
        this.router.navigate(['/categories'])
        location.reload();
      }
}


