import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { categoriesService } from '../../../services/inventory/categoriesService'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { productPopupComponent } from '../productPopup/productPopup.component'
import { shearedService } from '../../../services/shearedService'
import { Jsonp } from '@angular/http';
import * as $ from 'jquery'
import { DaliogService } from '../../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';



@Component({
    moduleId: module.id,
    selector: 'app-liveStock',
    templateUrl: 'liveStock.component.html'
})

export class liveStockComponent implements OnInit {

    constructor(private categoriesS: categoriesService, private route: ActivatedRoute, private dailog: MatDialog,
        private router: Router, private shearedS: shearedService, private customDialogService: DaliogService,
        private translate: TranslateService) {
    }

    public loading = false;
    id: any = this.route.snapshot.params['id']
    catItems: any = [];
    productDialog: MatDialogRef<productPopupComponent>
    categories: any = [];
    selectedItem: any;
    lang = localStorage.getItem('lang');
    currentPage: number = 1;
    totalItems: number;
    pageSize: number = 10;
    optionsResult: any = JSON.parse(localStorage.getItem('resultOption'));
    resultConfigId = JSON.parse(localStorage.getItem('resultConfigId'))
    optionId: any = localStorage.getItem('optionId');
    categoryConfigId: any;
    countryCode = localStorage.getItem('countryCode');
    cartStorage: any = [];
    cartQuantityCount: any = 0;
    acceptMulti: any;
    cartItemStorage = JSON.parse(localStorage.getItem('cartItems'));
    catNameEN: string = '';
    catNameAR: string = '';


    ngOnInit() {
        this.getCatById();
        this.categoriesS.getAllCategories(this.countryCode).subscribe(allCat => {
            this.categories = allCat.data;
        }, error => { console.log('error') })

        if (this.optionsResult) {
            if (this.optionsResult.length > 0) {
                this.optionId = this.optionsResult[0].id;
                localStorage.setItem('optionId', this.optionId)
            }
            else {
                this.optionId = 0
                localStorage.setItem('optionId', this.optionId)
            }
        }

    }

    changeOption(optionId) {
        this.optionId = optionId;
        localStorage.setItem('optionId', this.optionId)
    }

    getCatById() {
        this.loading = true;
        this.categoriesS.getCategorieById(this.countryCode, this.id).subscribe(catItems => {
            this.catItems = catItems.data;
            this.totalItems = this.catItems.length;
            this.loading = false;
        }, error => {
            console.error();
        })
    }

    addProduct(item) {
        this.productDialog = this.dailog.open(productPopupComponent, {
            width: '900px',
            height: '450px',
            data: item
        })
        this.productDialog.afterClosed().subscribe((result) => {
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;

        })
    }

    changeCat(catId, catNameEN, catNameAR, config) {
        this.loading = true;
        this.selectedItem = catId;

        if (config) {
            localStorage.setItem('resultConfigId', config.id)
            localStorage.setItem('resultOption', JSON.stringify(config.options))
            localStorage.setItem('acceptMulti', config.acceptMulti)
        }


        // let isAcceptMulti Boolean  >> local storage

        // compare with el7aga ely b3mlha click

        /// fy case el false hn3ml check lel selected category in cart >> else disable



        this.optionsResult = JSON.parse(localStorage.getItem('resultOption'));

        this.cartStorage = JSON.parse(localStorage.getItem('cartItems'));

        if (this.cartStorage) {
            if (this.cartStorage.length != 0) {
                localStorage.setItem('acceptMultiStorage', this.cartStorage[0].acceptMulti)

                let acceptMultiItem = JSON.parse(localStorage.getItem('acceptMulti'));
                let acceptMultiStorage = JSON.parse(localStorage.getItem('acceptMultiStorage'));
                console.log(typeof acceptMultiItem)
                console.log(typeof acceptMultiStorage)

                if (this.cartStorage[0].item.fK_Category_Id == catId) {
                    this.categoriesS.getCategorieById(this.countryCode, catId).subscribe(catItems => {
                        this.catItems = catItems.data;
                    }, error => {
                        console.error();
                    })
                    this.router.navigate(['/categories/' + catId]);
                    this.loading = false;
                } 
                else if (acceptMultiItem == true && acceptMultiStorage == true) {
                    this.categoriesS.getCategorieById(this.countryCode, catId).subscribe(catItems => {
                        this.catItems = catItems.data;
                    }, error => {
                        console.error();
                    })
                    this.router.navigate(['/categories/' + catId]);
                    this.loading = false;
                }
                else if (acceptMultiItem == false && acceptMultiStorage == false) {
                    this.loading = false;
                    this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                    this.optionsResult = [];
                }
                else if (acceptMultiItem != acceptMultiStorage) {
                    this.loading = false;
                    this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                    this.optionsResult = [];
                }
            }
                else {
                    this.categoriesS.getCategorieById(this.countryCode, catId).subscribe(catItems => {
                        this.catItems = catItems.data;
                    }, error => {
                        console.error();
                    })
                    this.router.navigate(['/categories/' + catId])
                    this.loading = false;
                }

                // if (this.cartStorage.length != 0) {
                //     for (let i = 0; i < this.cartStorage.length; i++) {
                //         if (this.cartStorage[i].item.fK_Category_Id == catId) {
                //             this.categoriesS.getCategorieById(this.countryCode, catId).subscribe(catItems => {
                //                 this.catItems = catItems.data;
                //             }, error => {
                //                 console.error();
                //             })
                //             this.router.navigate(['/categories/' + catId])
                //             this.loading = false;
                //         }
                //         else if ((this.cartStorage[i].acceptMulti == acceptMulti) == true) {
                //             this.categoriesS.getCategorieById(this.countryCode, catId).subscribe(catItems => {
                //                 this.catItems = catItems.data;
                //             }, error => {
                //                 console.error();
                //             })
                //             this.router.navigate(['/categories/' + catId]);
                //             this.loading = false;
                //         }
                //         else if ((this.cartStorage[i].acceptMulti == acceptMulti) == false) {
                //             this.loading = false;
                //             this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                //             this.optionsResult = [];
                //             break;
                //         }
                //         else if (this.cartStorage[i].acceptMulti != acceptMulti) {
                //             this.loading = false;
                //             this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                //             this.optionsResult = [];
                //             break;
                //         }
                //     }
                // }
                // else {
                //     this.categoriesS.getCategorieById(this.countryCode, catId).subscribe(catItems => {
                //         this.catItems = catItems.data;
                //     }, error => {
                //         console.error();
                //     })
                //     this.router.navigate(['/categories/' + catId])
                //     this.loading = false;
                // }
            }
        
        else {
            this.categoriesS.getCategorieById(this.countryCode, catId).subscribe(catItems => {
                this.catItems = catItems.data;
            }, error => {
                console.error();
            })
            this.router.navigate(['/categories/' + catId])
            this.loading = false;
        }

    }


    addOneItem(item, quantity) {
        this.cartStorage = localStorage.getItem('cartItems');
        this.cartQuantityCount = parseInt(localStorage.getItem('cartQuantityCount'))
        this.acceptMulti = localStorage.getItem('acceptMulti')
        if (this.cartStorage == "" || this.cartStorage == null) {
            this.cartStorage = new Array();
            this.cartStorage.push({
                item: item, cutting: 1, quantity: 1, configId: localStorage.getItem('resultConfigId'),
                optionId: localStorage.getItem('optionId'), acceptMulti: localStorage.getItem('acceptMulti')
            });
            localStorage.setItem('cartItems', JSON.stringify(this.cartStorage))
        } else {
            this.cartStorage = JSON.parse(localStorage.getItem('cartItems'));
            this.cartQuantityCount = parseInt(localStorage.getItem('cartQuantityCount'))
            this.acceptMulti = JSON.parse(localStorage.getItem('acceptMulti'))
            this.cartStorage.push({ item: item, cutting: 1, quantity: 1 });
            localStorage.setItem('cartItems', JSON.stringify(this.cartStorage))
        }

        if (!quantity.value) {
            quantity.value = '1'
        }
        this.cartQuantityCount += quantity.value;
        localStorage.setItem('cartQuantityCount', this.cartQuantityCount)

    }


    itemsPerPage: number;
    page: any;
    previousPage: any;

    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
        }
    }


}


