import {Observable} from 'rxjs/Rx';
import {MatDialogRef, MatDialog, MatDialogConfig} from '@angular/material';
import {Injectable} from'@angular/core';
import {ConfirmDialogComponent} from '../pages/confirmMsg/confirmMsg'
@Injectable()


export class DaliogService{
    constructor(private dialog: MatDialog) { }
    public confirm(title,messages,showCancelButton):Observable<boolean>{
        let dialogRef=this.dialog.open(ConfirmDialogComponent)
        dialogRef.componentInstance.title=title
        dialogRef.componentInstance.message=messages
        dialogRef.componentInstance.cancelButton=showCancelButton
        return dialogRef.afterClosed()

    }
    public alert(title,message,showCancelButton):Observable<boolean>{
        let dialogAlet=this.dialog.open(ConfirmDialogComponent)
        dialogAlet.componentInstance.title=title
        dialogAlet.componentInstance.message=message
        dialogAlet.componentInstance.cancelButton=showCancelButton
        return dialogAlet.afterClosed()
    }


}
