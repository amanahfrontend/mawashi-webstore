import { Component, OnInit, Inject, Input } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { categoriesService } from '../../../services/inventory/categoriesService';

import { shearedService } from '../../../services/shearedService';
import * as $ from 'jquery'
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';



@Component({
    moduleId: module.id,
    selector: 'app-productPopup',
    templateUrl: 'productPopup.component.html'
})


export class productPopupComponent implements OnInit {

    constructor(private router: Router, public dialogRef: MatDialogRef<productPopupComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
        private categoriesS: categoriesService, private shearedS: shearedService) {

    }

    public loading = false;
    cartStorage: any = [];
    productDetailes: any = {}
    quantityNum: any = {}
    pieceNum: any = {};
    cartQuantityCount: any = 0;
    cartQuantityCount2: any = 0;
    acceptMulti: any;
    countryCode = localStorage.getItem('countryCode');
    lang = localStorage.getItem('lang');

    form = new FormGroup({
        quantityNum: new FormControl(null, Validators.required),
        pieceNum: new FormControl(null, Validators.required)
    });


    ngOnInit() {
        this.getProductDetailsById();
    }


    getProductDetailsById() {
        this.categoriesS.getProductById(this.countryCode, this.data.id).subscribe(itemDetails => {
            this.productDetailes = itemDetails.data;
        }, error => {
            console.log('error')
        })

    }


    addProductToCart(item, cut, quantity) {

      // if dh m4 mawgood   localStorage.getItem('acceptMulti', config.acceptMulti)

      // set lel accept multi   && incase false >>> set el category id { cart acceptance >> object >>> { accept multi : boolean , category id : "id"} }

    //   localStorage.setItem('acceptMulti', config.acceptMulti)
      

        cut = this.pieceNum;
        quantity = this.quantityNum;

        this.cartStorage = localStorage.getItem('cartItems');
        this.cartQuantityCount = parseInt(localStorage.getItem('cartQuantityCount'));
        this.acceptMulti = localStorage.getItem('acceptMulti')
        if (this.cartStorage == "" || this.cartStorage == null) {
            this.cartStorage = new Array();

            this.cartStorage.push({
                item: item, cutting: this.pieceNum, quantity: this.quantityNum, configId: localStorage.getItem('resultConfigId'),
                optionId: localStorage.getItem('optionId'), acceptMulti: localStorage.getItem('acceptMulti')
            });
            localStorage.setItem('cartItems', JSON.stringify(this.cartStorage))
            localStorage.setItem('itemId', item.id)

        }
        else {
            this.cartStorage = JSON.parse(localStorage.getItem('cartItems'));
            this.cartQuantityCount = parseInt(localStorage.getItem('cartQuantityCount'));
            this.acceptMulti = JSON.parse(localStorage.getItem('acceptMulti'))
            let itemIsFound = false;
            for (let i = 0; i < this.cartStorage.length; i++) {
                if (this.cartStorage[i].item.id == item.id) {
                    this.cartStorage[i].quantity = quantity
                    this.cartStorage[i].cutting = cut
                    itemIsFound = true;
                    localStorage.setItem('cartItems', JSON.stringify(this.cartStorage))
                    this.cartQuantityCount = quantity.value;
                    localStorage.setItem('cartQuantityCount', this.cartQuantityCount);

                    this.shearedS.cartCount = this.cartQuantityCount;
                }
            }
            if (!itemIsFound) {
                this.cartStorage.push({
                    item: item, cutting: this.pieceNum, quantity: this.quantityNum, configId: localStorage.getItem('resultConfigId'),
                    optionId: localStorage.getItem('optionId'), acceptMulti: localStorage.getItem('acceptMulti')
                });
                localStorage.setItem('cartItems', JSON.stringify(this.cartStorage))
                localStorage.setItem('itemId', item.id)
                // this.cartQuantityCount += quantity.value;
                // localStorage.setItem('cartQuantityCount', this.cartQuantityCount);

                // this.shearedS.cartCount = this.cartQuantityCount;

            }
        }

        this.dialogRef.close();
        this.dialogRef.afterClosed().subscribe(() => {
            localStorage.setItem('cartItems', JSON.stringify(this.cartStorage))
            localStorage.setItem('itemId', item.id)
            this.loading = true;
            if (localStorage.getItem('cartQuantityCount') != null) {
                this.cartQuantityCount = parseInt(localStorage.getItem('cartQuantityCount'))
            } else {
                this.cartQuantityCount = 0;
            }
            this.cartQuantityCount += quantity.value;
            localStorage.setItem('cartQuantityCount', this.cartQuantityCount);

            this.shearedS.cartCount = this.cartQuantityCount;
        })
    }

}
