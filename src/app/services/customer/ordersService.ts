import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class ordersService {
    constructor(private http: Http, private config: webService) {
    }

    getAllOrders(countryCode): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}', countryCode) + this.config.getAllOrdersUrl
        return this.http.get(url, { headers: this.config.headers })
            .map(response => response.json())
    }
    getOrderByCustomer(countryCode,custId):Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}', countryCode) + this.config.getOrderByCustomerUrl.replace('{1}', custId)
        return this.http.get(url, { headers: this.config.headers })
            .map(response => response.json())
    }
    getOrderDeliveryFees(countryCode):Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}', countryCode) + this.config.getOrderDeliveryFeesUrl
        return this.http.get(url, { headers: this.config.headers })
            .map(response => response.json())
    }
    getLocations_Dates(countryCode,obj):Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}', countryCode) + this.config.getLocations_DatesUrl
        return this.http.post(url,obj, { headers: this.config.headers })
            .map(response => response.json())
    }
    getPaymentStatus(countryCode,orderId):Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}', countryCode) + this.config.getPaymentStatusUrl.replace('{1}', orderId)
        return this.http.get(url, { headers: this.config.headers })
            .map(response => response.json())
    }
    getOrderCount(countryCode,orderId):Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}', countryCode) + this.config.getOrderCount.replace('{1}', orderId)
        return this.http.get(url, { headers: this.config.headers })
            .map(response => response.json())
    }
    getOrderById(countryCode,orderId):Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}', countryCode) + this.config.getOrderByIdUrl.replace('{1}', orderId)
        return this.http.get(url, { headers: this.config.headers })
            .map(response => response.json())
    }
}