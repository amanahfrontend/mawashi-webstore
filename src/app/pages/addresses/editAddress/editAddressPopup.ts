import { Component, OnInit, ViewChild, ElementRef, NgModule, NgZone, ViewContainerRef, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { locationsService } from '../../../services/location/locationService'
import { addressService } from '../../../services/customer/addressService'
import { accountService } from '../../../services/customer/customerAccount'
import { DaliogService } from '../../../services/confirmMsg';
import { AgmCoreModule, MapsAPILoader, AgmMap } from '@agm/core';
import { } from '@types/googlemaps';
import { ignoreElements } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';

declare var google: any;



@Component({
    moduleId: module.id,
    selector: 'app-editAddress',
    templateUrl: 'editAddressPopup.html'
})

export class editAddressComponent implements OnInit {

    constructor(private locationsS: locationsService, private addressS: addressService, private accountS: accountService,
        private customDialogService: DaliogService, private route: ActivatedRoute, public dialogRef: MatDialogRef<editAddressComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any, private router: Router, private mapsAPILoader: MapsAPILoader, private translate: TranslateService,
        private ngZone: NgZone, ) {
    }

    public loading = false;
    country = localStorage.getItem('country')
    addressObj: any = {};
    // locationPaciResult: any = {};
    locationMkaniResult: any = { governorate: {}, area: {}, block: {}, street: {} };
    locationPaciResult: any = { governorate: {}, area: {}, block: {}, street: {} };
    locationPaciNum: number;
    locationMkaniNum: number;
    addressName: string = ''
    governorates: any = [];
    governorateId: string = '';
    areas: any = [];
    areaId: string = '';
    blocks: any = [];
    blockId: string = ''
    streets: any = [];
    streetId: string = '';

    public latitude: number = 29.364074;
    public longitude: number = 47.967134;
    public zoom: number = 8;


    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
                if (res) {
                    this.router.navigate(['/login'])
                }
            })
        }

        // this.locationMkaniResult = { governorate: {}, area: {}, block: {}, street: {} };
        this.getAddressById();
        this.getAllGovernorates();
        // this.setCurrentPosition();
        this.getAllAreas();
        this.getAllBlocks();
        this.getAllStreets();
    }

    markerDragEnd($event) {
        console.log($event);
        let geocoder = new google.maps.Geocoder();
        this.latitude = $event.coords.lat;
        this.longitude = $event.coords.lng;
        geocoder.geocode({ 'location': { lat: this.latitude, lng: this.longitude } }, (results, status) => {
            if (status === 'OK') {
                console.log(results)
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

    setCurrentPosition() {
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 12;
            });
        }
    }

    getAddressById() {
        this.loading = true;
        if (this.data.id) {
            this.addressS.getAddressById(this.data.id).subscribe(result => {
                // this.addressObj = result.data;
                console.log("result.data");
                console.log(result.data);
                if (result.data.number) {
                    this.locationMkaniNum = result.data.number
                    this.locationPaciNum = result.data.number
                }
                if (result.data.name) {
                    this.addressName = result.data.name
                }
                if (result.data.governorateName) {
                    this.loading = true;
                    this.locationMkaniResult.governorate.name = result.data.governorateName
                    // this.governorateId = result.data.governorateName
                    this.locationPaciResult.governorate.name = result.data.governorateName
                }
                if (result.data.areaName) {
                    this.loading = true;
                    this.locationMkaniResult.area.name = result.data.areaName
                    // this.areaId = result.data.areaName;
                    // this.areas = this.getAllAreas();
                    this.locationPaciResult.area.name = result.data.areaName
                }
                if (result.data.blockName) {
                    this.loading = true;
                    this.locationMkaniResult.block.name = result.data.blockName;
                    // this.blockId = result.data.blockName;
                    // this.blocks = this.getAllBlocks();
                    this.locationPaciResult.block.name = result.data.blockName;
                }
                if (result.data.streetName) {
                    this.loading = true;
                    this.locationMkaniResult.street.name = result.data.streetName
                    // this.streetId = result.data.streetName
                    // this.streets = this.getAllStreets();
                    // console.log(this.streets);
                    this.locationPaciResult.street.name = result.data.streetName
                }
                if (result.data.latitude && result.data.longitude) {
                    this.loading = true;
                    navigator.geolocation.getCurrentPosition((position) => {
                        this.latitude = result.data.latitude;
                        this.longitude = result.data.longitude;
                        this.zoom = 10;
                    });
                }
                if (result.data.building) {
                    this.loading = true;
                    this.locationMkaniResult.building = result.data.building;
                    this.locationPaciResult.building = result.data.building
                }
                if (result.data.floor) {
                    this.loading = true;
                    this.locationMkaniResult.floor = result.data.floor;
                    this.locationPaciResult.floor = result.data.floor;
                }
                if (result.data.appartmentNumber) {
                    this.loading = true;
                    this.locationMkaniResult.appartmentNumber= result.data.appartmentNumber;
                    this.locationPaciResult.appartmentNumber= result.data.appartmentNumber
                }
                if (result.data.deliveryNote) {
                    this.loading = true;
                    this.locationMkaniResult.deliveryNote = result.data.deliveryNote;
                    this.locationPaciResult.deliveryNote = result.data.deliveryNote;
                }
                this.loading = false;
            }, error => {
                console.log('error')
            })
        }
        else {
            this.setCurrentPosition();
        }

    }

    getLocationByMakaniNumber(mkaniNum, event) {
        this.loading = true;
        this.locationsS.getMakaniInfoFromNumber(mkaniNum).subscribe(responseResult => {
            this.loading = false;
            if (responseResult == null) {
                this.locationMkaniResult = { governorate: {}, area: {}, block: {}, street: {} };
                this.loading = false;
                this.customDialogService.alert(this.translate.instant('eror'), this.translate.instant('makniNoinvalid'), false);
            }
            else {
                if (responseResult.governorate) {
                    this.locationMkaniResult.governorate.name = responseResult.governorate.name
                }
                if (responseResult.area) {
                    this.locationMkaniResult.area.name = responseResult.area.name
                }
                if (responseResult.block) {
                    this.locationMkaniResult.block.name = responseResult.block.name
                }
                if (responseResult.street) {
                    this.locationMkaniResult.street.name = responseResult.street.name
                }
                else {
                    this.locationMkaniResult.street = {};
                    this.loading = false;
                }
                if (responseResult.lat && responseResult.lng) {
                    this.loading = true;
                    navigator.geolocation.getCurrentPosition((position) => {
                        this.latitude = responseResult.lat;
                        this.longitude = responseResult.lng;
                        this.zoom = 12;
                    });
                    this.loading = false;
                }
                if (responseResult.building) {
                    this.locationMkaniResult.building = responseResult.building
                }
                if (responseResult.floor) {
                    this.locationMkaniResult.floor = responseResult.floor
                }
                if (responseResult.appartmentNumber) {
                    this.locationMkaniResult.appartmentNumber = responseResult.appartmentNumber
                }
                if (responseResult.deliveryNote) {
                    this.locationMkaniResult.appartmentNumber = responseResult.deliveryNote
                }
            }

        }, error => {
            this.loading = false;
            console.log('error')
        })

    }

    getLocationByPaciNUmNew(paciNum) {
        this.loading = true;
        this.locationsS.getPaciByNum(paciNum).subscribe(responseResult => {
            this.loading = false;
            if (responseResult == null) {
                this.locationPaciResult = { governorate: {}, area: {}, block: {}, street: {} };
                this.loading = false;
                this.customDialogService.alert(this.translate.instant('eror'), this.translate.instant('paciNuminvalid'), false);
            }
            else {
                if (responseResult.governorate) {
                    this.locationPaciResult.governorate.name = responseResult.governorate.name
                }
                if (responseResult.area) {
                    this.locationPaciResult.area.name = responseResult.area.name
                }
                if (responseResult.block) {
                    this.locationPaciResult.block.name = responseResult.block.name
                }
                if (responseResult.street) {
                    this.locationPaciResult.street.name = responseResult.street.name
                }
                else {
                    this.locationPaciResult.street = {};
                    this.loading = false;
                }
                if (responseResult.lat && responseResult.lng) {
                    navigator.geolocation.getCurrentPosition((position) => {
                        this.latitude = responseResult.lat;
                        this.longitude = responseResult.lng;
                        this.zoom = 12;
                    });
                }
                if (responseResult.building) {
                    this.locationPaciResult.building = responseResult.building
                }
                if (responseResult.floor) {
                    this.locationPaciResult.floor = responseResult.floor
                }
                if (responseResult.appartmentNumber) {
                    this.locationPaciResult.appartmentNumber = responseResult.appartmentNumber
                }
                if (responseResult.deliveryNote) {
                    this.locationPaciResult.deliveryNote = responseResult.deliveryNote
                }
            }

        }, error => {
            this.loading = false;
            console.log('error')
        })
    }
    getLocationByPaciNUm(paciNum) {
        this.loading = true;
        this.locationsS.getPaciByNum(paciNum).subscribe(responseResult => {
            if (responseResult) {
                this.locationsS.getAllGovernorates().subscribe(allGovernorates => {
                    this.governorates = allGovernorates;
                    this.locationsS.getAllAreas(responseResult.governorate.id).subscribe(allAreas => {
                        this.areas = allAreas;
                        this.locationsS.getAllBlocks(responseResult.area.id).subscribe(allBlocks => {
                            this.blocks = allBlocks;
                            if (responseResult.governorate.id && responseResult.area.id && responseResult.block.name) {
                                this.locationsS.getAllStreets(responseResult.governorate.id, responseResult.area.id, responseResult.block.name).subscribe(allStreets => {
                                    this.loading = true;
                                    this.streets = allStreets;

                                    if (responseResult.governorate) {
                                        this.governorateId = responseResult.governorate.name
                                    }
                                    if (responseResult.area) {
                                        this.areaId = responseResult.area.name
                                    }
                                    if (responseResult.block) {
                                        this.blockId = responseResult.block.name
                                    }
                                    if (responseResult.street) {
                                        this.streetId = responseResult.street.name
                                    }
                                    if (responseResult.lat && responseResult.lng) {
                                        navigator.geolocation.getCurrentPosition((position) => {
                                            this.latitude = responseResult.lat;
                                            this.longitude = responseResult.lng;
                                            this.zoom = 12;
                                        });
                                    }

                                    this.loading = false;
                                }, error => {
                                    this.loading = false;
                                    console.log('error')
                                })
                            }
                        }, error => {
                            this.loading = false;
                            console.log('error')
                        })
                    }, error => {
                        this.loading = false;
                        console.log('error')
                    })
                }, error => {
                    this.loading = false;
                    console.log('error')
                })

            }
            else {
                this.loading = false;
                this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('noDataForNum'), false);
            }
        }, error => {
            this.loading = false;
            console.log('error')
        })
    }

    getAllGovernorates() {
        this.locationsS.getAllGovernorates().subscribe(allGovernorates => {
            this.governorates = allGovernorates;
        }, error => {
            console.log('error')
        })
    }

    getAllAreas() {
        this.loading = true;
        this.locationsS.getAllAreas(this.governorateId).subscribe(allAreas => {
            this.areas = allAreas;
            this.loading = false;
        }, error => {
            this.loading = false;
            console.log('error')
        })
    }

    getAllBlocks() {
        this.loading = true;
        this.locationsS.getAllBlocks(this.areaId).subscribe(allBlocks => {
            this.blocks = allBlocks;
            this.loading = false;
        }, error => {
            this.loading = false;
            console.log('error')
        })
    }
    getAllStreets() {
        this.loading = true;
        this.locationsS.getAllStreets(this.governorateId, this.areaId, this.blockId).subscribe(allStreets => {
            this.streets = allStreets;
            this.loading = false;
        }, error => {
            console.log('error')
            this.loading = false;
        })
    }

    addEditAddress() {
        this.addressObj.Fk_Customer_Id = this.data.currentUserId
        this.addressObj.name = this.addressName;
        this.addressObj.latitude = this.latitude;
        this.addressObj.longitude = this.longitude;

        if (this.country == '2') {
            /////mkani uae
            this.addressObj.Fk_Country_Id = 2;
            this.addressObj.fk_AddressType_Id = 2,
            this.addressObj.number = this.locationMkaniNum;
            this.addressObj.GovernorateName = this.locationMkaniResult.governorate.name
            this.addressObj.GovernorateId = this.locationMkaniResult.governorate.id
            this.addressObj.AreaName = this.locationMkaniResult.area.name
            this.addressObj.AreaId = this.locationMkaniResult.area.id
            this.addressObj.BlockName = this.locationMkaniResult.block.name
            this.addressObj.BlockId = this.locationMkaniResult.block.id
            if (this.locationMkaniResult.street) {
                this.addressObj.StreetName = this.locationMkaniResult.street.name
                this.addressObj.StreetId = this.locationMkaniResult.street.id
            }
            this.addressObj.building = this.locationMkaniResult.building
            this.addressObj.floor = this.locationMkaniResult.floor
            this.addressObj.appartmentNumber = this.locationMkaniResult.appartmentNumber
            this.addressObj.deliveryNote = this.locationMkaniResult.deliveryNote
        }

        if (this.country == '1') {
            ////paci kw
            this.addressObj.Fk_Country_Id = 1;
            this.addressObj.fk_AddressType_Id = 1,
            this.addressObj.number = this.locationPaciNum;
            this.addressObj.GovernorateName = this.locationPaciResult.governorate.name
            this.addressObj.GovernorateId = this.locationPaciResult.governorate.id
            this.addressObj.AreaName = this.locationPaciResult.area.name
            this.addressObj.AreaId = this.locationPaciResult.area.id
            this.addressObj.BlockName = this.locationPaciResult.block.name
            this.addressObj.BlockId = this.locationPaciResult.block.id
            if (this.locationPaciResult.street) {
                this.addressObj.StreetName = this.locationPaciResult.street.name
                this.addressObj.StreetId = this.locationPaciResult.street.id
            }
            this.addressObj.building = this.locationPaciResult.building
            this.addressObj.floor = this.locationPaciResult.floor
            this.addressObj.appartmentNumber = this.locationPaciResult.appartmentNumber
            this.addressObj.deliveryNote = this.locationPaciResult.deliveryNote
        }

        if (this.data.id) {
            this.addressObj.id = this.data.id
            this.addressS.editAddress(this.addressObj).subscribe(result => {
                this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('addrsAddSuces'), false);
                this.dialogRef.close();
                console.log(JSON.stringify(this.addressObj))
            }, error => {
                this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('addrsAddFail'), false);

            })
        }
        else {
            this.addressS.postAddress(this.addressObj).subscribe(result => {
                this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('addrsAddSuces'), false);
                this.dialogRef.close();
                // this.addressObj = {}
                // this.getAddressesByUser();    
            }, error => {
                this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('addrsAddFail'), false);

            })
        }

    }

}