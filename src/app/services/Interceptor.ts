import { Observable } from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { HttpErrorResponse } from "@angular/common/http";
import 'rxjs/add/operator/do';


@Injectable()
export class AngularInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).do(event => {}, err => {
        if(err instanceof HttpErrorResponse){ // here you can even check for err.status == 404 | 401 etc
            console.log("Error Caught By Interceptor");
            //Observable.throw(err); // send data to service which will inform the component of the error and in turn the user
        }
    });
  }
}