import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class paymentService {
    constructor(private http: Http, private config: webService) {
    }

    getPaymentMethods(countryCode,countryId,deliveryType): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicePaymentApi.replace('{0}',countryCode) + this.config.getPaymentMethodeUrl.replace('{1}',countryId).replace('{2}',deliveryType)
        return this.http.get(url, {headers:this.config.headers})
            .map(response => response.json())
    }
}