import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { addressService } from '../../../services/customer/addressService'
import { ordersService } from '../../../services/customer/ordersService'
import { categoriesService } from '../../../services/inventory/categoriesService'
import { DaliogService } from '../../../services/confirmMsg';
import * as $ from 'jquery'
import { cartService } from '../../../services/cart/cartService'
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../../services/shearedService';
import { ThrowStmt } from '@angular/compiler';


@Component({
    moduleId: module.id,
    selector: 'app-checkOutAddress',
    templateUrl: 'checkOutAddress.component.html'
})

export class checkOutAddressComponent implements OnInit {

    constructor(private categoriesS: categoriesService, private addressS: addressService, private ordersS: ordersService,
        private customDialogService: DaliogService, private router: Router, private cartS: cartService, private translate: TranslateService,
        private shearedS: shearedService) {
    }

    public loading = false;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    configId: any;
    country = localStorage.getItem('country')
    countryCode = localStorage.getItem('countryCode')
    addressId: any;
    addresses: any = [];
    deliveryOptions: any = [];
    deliverOptionResponsObj: any = {}
    DeliveryTypeId: string = '';
    deliveryConfigPostResult: any = {}
    minDate = new Date();
    deliverDate: Date;
    selectedDeliveryDate: Date;
    deliveryTypeId: string;
    togglePanel: any = {};
    cartResult: any = {};
    cartResultLength: any;
    lang = localStorage.getItem('lang');

    form = new FormGroup({
        deliveryDate: new FormControl(null, Validators.required),
        selectedDeliveryDate: new FormControl(null, Validators.required),
        deliveryTypeId: new FormControl(null, Validators.required),
        addressId: new FormControl(null, Validators.required)
    });

    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
                if (res) {
                    this.router.navigate(['/login'])
                }
            })
        }

        $('body').on('keydown', 'input', function (e) {
            if (e.which === 32 && e.target.selectionStart === 0) {
                return false;
            }
        });
        $('body').on('keydown', 'textarea', function (e) {
            if (e.which === 32 && e.target.selectionStart === 0) {
                return false;
            }
        });

        this.configId = localStorage.getItem("configId");
        this.GetDeliveryOptionsByCategoryConfigId(this.configId);

        this.shearedS.deliveryDate = new Date(this.shearedS.deliveryDate);
        this.getCurrentCart();
    }

    stringDate(date) {
        let d = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getUTCHours(),
            date.getUTCMinutes(),
            date.getUTCSeconds()));
        return d.toISOString().split('T')[0]
    }

    getCurrentCart() {
        this.cartS.getCartOverview(this.countryCode, this.userId).subscribe(result => {
            this.cartResult = result.data;
            this.cartResultLength = this.cartResult.cartItems.length;
        }, error => {
            console.log("Error");
        })
    }

    GetDeliveryOptionsByCategoryConfigId(id) {
        this.loading = true;
        this.categoriesS.GetDeliveryOptionsByCategoryConfigId(this.countryCode, id).subscribe(optionsResult => {
            this.deliveryOptions = optionsResult.data;
            this.loading = false;
        }, error => {
            console.log('error')
        })
    }

    getLocations_Dates(deliveryTypeId) {
        this.shearedS.deliveryTypeId = deliveryTypeId;
        this.loading = true;
        this.deliverOptionResponsObj.DeliveryType = this.shearedS.deliveryTypeId;
        this.deliverOptionResponsObj.FK_Customer_Id = this.userId;
        this.deliverOptionResponsObj.FK_Config_Id = this.configId;
        this.ordersS.getLocations_Dates(this.countryCode, this.deliverOptionResponsObj).subscribe(result => {
            this.deliveryConfigPostResult = result.data
            this.loading = false;

        }, error => {
            console.log('error')
        })
    }


    submitLocation_Date() {
        // localStorage.setItem('deliveryTypeId', this.deliveryTypeId)
        // localStorage.setItem('locationId', this.addressId)
        this.shearedS.deliveryTypeId;
        this.shearedS.locationId;

        // if (this.deliverDate) {
        //     // localStorage.setItem('deliverDate', this.deliverDate.toLocaleDateString())
        //     this.shearedS.deliveryDate = this.deliverDate.toLocaleDateString()
        //     this.router.navigate(['/checkOut/checkOutDelivery'])
        // }
        // else if (this.selectedDeliveryDate) {
        //     console.log(typeof this.selectedDeliveryDate)
        //     // localStorage.setItem('deliverDate', new Date(this.selectedDeliveryDate).toLocaleDateString())
        //     this.shearedS.deliveryDate = new Date(this.selectedDeliveryDate).toLocaleDateString()
        //     this.router.navigate(['/checkOut/checkOutDelivery'])
        // }

        if (this.shearedS.deliveryDate) {
            if (this.deliveryConfigPostResult.dates != null) {
                this.shearedS.deliveryDate
            }
            else {
                this.shearedS.deliveryDate = this.shearedS.deliveryDate.toLocaleDateString()

            }
            this.router.navigate(['/checkOut/checkOutDelivery'])
        }
        else {
            this.customDialogService.alert(this.translate.instant('wrng'), this.translate.instant('chosDelivryDate'), false);
        }

    }

}


