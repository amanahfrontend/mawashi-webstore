import { Component, OnInit, Inject, Input, ViewChild } from '@angular/core';
import { FormsModule, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { checkOutService } from '../../services/checkout/checkoutService';

import * as $ from 'jquery'
import { Jsonp } from '@angular/http';
import { Lightbox } from 'angular2-lightbox';
import { NgxImageGalleryComponent, GALLERY_IMAGE, GALLERY_CONF } from "ngx-image-gallery";

import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';



@Component({
    moduleId: module.id,
    selector: 'app-ads',
    templateUrl: 'ads.component.html'
})


export class adsComponent implements OnInit {

    constructor(private router: Router, private checkOutS: checkOutService, public dialogRef: MatDialogRef<adsComponent>,
        private _lightbox: Lightbox) {

    }

    galleryOptions: NgxGalleryOptions[];
    galleryImages: NgxGalleryImage[];

    country = localStorage.getItem('country');
    lang = localStorage.getItem('lang');
    ads: any = [];

    conf: GALLERY_CONF = {
        imageOffset: '0px',
        showDeleteControl: false,
        showImageTitle: false,
    };

    ngOnInit() {
        this.getAllAds();

        this.galleryOptions = [
            {
                width: '320px',
                height: '340px',
                imageAnimation: NgxGalleryAnimation.Slide,
                imageAutoPlay: true,
                thumbnails: false,
                imageArrowsAutoHide: true,

            }
        ];
    }

    getAllAds() {
        this.checkOutS.getAds(this.country).subscribe(result => {
            if (result.data && result.data.length > 0) {
                result.data.map((ad) => {
                    this.ads.push({
                        medium: ad,
                        big: ad
                    });
                });
            }
        }, error => {
            console.log('error')
        })
    }

}
