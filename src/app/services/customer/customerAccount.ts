import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class accountService {
    constructor(private http: Http, private config: webService) {
    }

    register(obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.registerNewCustomer
        return this.http.post(url, obj)
            .map(response => response.json())
    }
    UpdateCustomerUser(obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.UpdateCustomerUser
        return this.http.put(url, obj,{headers:this.config.headers})
            .map(response => response.json())
    }

    login(username: string, password: string) {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.loginUser
         return this.http.post(url, { username: username, password: password })
             .map((response) => { 
                 let user = response.json();
                 if (user) {
                     localStorage.setItem('mwashi_currentAuthorzation', JSON.stringify(user.token));
                     localStorage.setItem('userName', JSON.stringify(user.userName));
                     localStorage.setItem('userId', JSON.stringify(user.userId));                    
                 }
                 return response.json();
             });
     }

    getUserById(userId): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.getUserById.replace('{0}',userId)
        return this.http.get(url, {headers: this.config.headers})
            .map(response => response.json())
    }

    getCustomerById(customerId): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.getCustomerById.replace('{0}',customerId)
        return this.http.get(url,{headers:this.config.headers})
            .map(response => response.json())
    }

    isUsernameExist(userNmae): Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesidentity + this.config.isUsernameExist.replace('{0}',userNmae)
        return this.http.get(url,{headers:this.config.headers})
            .map(response => response.json())
    }

    isEmailExist(mail): Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesidentity + this.config.isEmailExist.replace('{0}',mail)
        return this.http.get(url,{headers:this.config.headers})
            .map(response => response.json())
    }

    generateForgetPasswordToken(userNmae): Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesidentity + this.config.generateForgetPasswordToken.replace('{0}',userNmae)
        return this.http.get(url)
            .map(response => response.json())
    }

    changePassword(obj): Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesidentity + this.config.changePassword
        return this.http.post(url,obj)
            .map(response => response.json())
    }
    forgetPassword(obj): Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesidentity + this.config.forgetPasswordUrl
        return this.http.post(url,obj)
            .map(response => response.json())
    }

    getPhoneVerficationToken(obj): Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesidentity + this.config.getPhoneVerficationToken
        return this.http.post(url,obj)
            .map(response => response.json())
    }

    confirmPhone(obj): Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesidentity + this.config.confirmPhone
        return this.http.post(url,obj)
            .map(response => response.json())
    }
}