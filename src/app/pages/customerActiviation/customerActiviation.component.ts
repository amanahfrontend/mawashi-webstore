import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { accountService } from '../../services/customer/customerAccount';
import { ordersService } from '../../services/customer/ordersService';
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../services/shearedService';


@Component({
    moduleId: module.id,
    selector: 'app-customerActiviation',
    templateUrl: 'customerActiviation.component.html'
})

export class custActivationComponent implements OnInit {

    constructor(private accountS:accountService, private customDialogService: DaliogService,private router: Router,
        private translate: TranslateService, private rout: ActivatedRoute, private sheardS: shearedService) {
    }

    public loading = false;
    userName: any = localStorage.getItem('registerUserName')
    phone: any = localStorage.getItem('phone')
    activObj: any = {};
    reSendObj: any = {};
    country = localStorage.getItem('country');
    previousUrl: any;

    
    ngOnInit() {
    
    }

    generateactiveCode(){
        this.reSendObj.userName = this.userName;
        this.reSendObj.phone = this.phone;
        this.reSendObj.country = this.country;
        this.accountS.getPhoneVerficationToken(this.reSendObj).subscribe(result =>{
            this.customDialogService.alert('Done', 'please wait for sms on your phone', false);
        }, error => {
            this.customDialogService.alert('Fail', 'some thing wrong', false);

        })
    }

    activeCustomer(){
        this.activObj.userName = this.userName;
        this.activObj.phone = this.phone;
        this.accountS.confirmPhone(this.activObj).subscribe(result =>{
            this.router.events
            .filter((event) => event instanceof NavigationEnd)
            .map(() => this.rout)
            .subscribe((event) => {
                this.previousUrl = event.url;
                localStorage.setItem('previousUrl',this.previousUrl._value[0].path)
            });

            this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('activMsg'), false);
            this.router.navigate(['login'])
        }, error => {
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('activFil'), false);

        })
    }
    
    

}


