import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ordersService } from '../../services/customer/ordersService';
import { accountService } from '../../services/customer/customerAccount'
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../services/shearedService';



@Component({
    moduleId: module.id,
    selector: 'app-allOrders',
    templateUrl: 'allOrders.component.html'
})

export class allOrdersComponent implements OnInit {

    constructor( private ordersS: ordersService, private accountS:accountService,private router: Router, private translate: TranslateService,
        private customDialogService: DaliogService, private sheardS:shearedService) {

    }

    allOrders: any = [];
    currentUser: any = []
    loading: boolean = false;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    countryCode = localStorage.getItem('countryCode');
    ordersCount: number;
    currentPageIdx: number = 1;
    totalItems: number;
    itemPerPage: number = 10;
    offset: number = 0;
    limit: number = 10;


    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
              if (res) {
                this.router.navigate(['/login'])
              }
            })
          }

        this.getOrders();
        this.getCurrentUser();
    }

    getCurrentUser(){
        this.accountS.getUserById(this.userId).subscribe(result =>{
            this.currentUser = result.data;
        },error =>{
            if(error.status == 401){
            console.log('error')
            }
            console.log('error')
        })
    }

    getOrders(){
        this.loading = true;
        this.ordersS.getOrderCount(this.countryCode,this.userId).subscribe(count =>{
            this.ordersCount = count.data
        this.ordersS.getOrderByCustomer(this.countryCode,this.userId).subscribe(result =>{
            this.allOrders = result.data
            this.loading = false;
        },error => {
            console.log('error')
        })
    },error => {
        console.log('error')
    })
    }


    pageChanged($event){
        this.loading = true;
        this.currentPageIdx = this.currentPageIdx + 1;
        this.offset = this.currentPageIdx * this.itemPerPage;
        this.getOrders()
    }

    viewOrder(order){
        this.router.navigate(['orders/detail/' + order.id])
    }

    logOut() {
        localStorage.removeItem('mwashi_currentAuthorzation');
        localStorage.removeItem('userName');
        localStorage.removeItem('userId')
        localStorage.removeItem('acceptMulti');
        localStorage.removeItem('cartItems');
        localStorage.removeItem('cartItemsId')
        localStorage.removeItem('cartQuantityCount');
        localStorage.removeItem('itemId');
        localStorage.removeItem('optionId');
        localStorage.removeItem('configId');
        localStorage.removeItem('resultConfigId');
        localStorage.removeItem('resultOption');
        localStorage.removeItem('order#');
        localStorage.removeItem('phone');
        localStorage.removeItem('registerUserName')
        localStorage.removeItem("customerActiviation")
        this.sheardS.cartCount = 0;
    
        this.router.navigate(['/categories'])
        location.reload();
      }

}


