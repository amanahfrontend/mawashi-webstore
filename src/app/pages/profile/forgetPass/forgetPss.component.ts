import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { accountService } from '../../../services/customer/customerAccount';
import { ordersService } from '../../../services/customer/ordersService';
import { DaliogService } from '../../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';


@Component({
    moduleId: module.id,
    selector: 'app-forgetPss',
    templateUrl: 'forgetPss.component.html'
})

export class forgetPssComponent implements OnInit {

    constructor(private accountS: accountService, private customDialogService: DaliogService, private router: Router,
        private ordersS: ordersService, private translate: TranslateService) {
    }

    public loading = false;
    userName: string = '';
    passObj: any = {};
    ordersCount: number;
    userId: any = JSON.parse(localStorage.getItem('userId'));
    countryCode = localStorage.getItem('countryCode');
    showForgetDiv: boolean = false;

    ngOnInit() {
    }

    generateForgetPasswordToken() {
        this.accountS.isUsernameExist(this.userName).subscribe(result => {
            if (result.data == true) {
                this.accountS.generateForgetPasswordToken(this.userName).subscribe(result => {
                    this.customDialogService.alert('Done', 'please wait for sms on your phone', false);
                    this.showForgetDiv = true;
                }, error => {
                    this.customDialogService.alert('Fail', 'some thing wrong', false);
                })
            }
            else{
                this.customDialogService.alert('fail', 'this user name is not exist', false);
            }
        })

    }

    changePass() {
        this.passObj.username = this.userName;
        this.accountS.forgetPassword(this.passObj).subscribe(result => {
            if (result.data == true) {
                this.customDialogService.alert('Done', 'Your Password Changed successfully', false);
                this.router.navigate(['/login'])
            }
        }, error => {
            this.customDialogService.alert('Fail', 'some thing wrong while Change Your Password', false);

        })
    }


}


