import { ModuleWithProviders }  from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//////Component//////
import { homeComponent } from './pages/home/home.component';
import { categoriesComponent } from './pages/categories/categories.component';
import { liveStockComponent } from './pages/categories/liveStock/liveStock.component';
import { productDetailsComponent } from './pages/categories/productDetails/productDetails.component';
import { shoppingCartComponent } from './pages/shoppingCart/shoppingCart.component';
import { checkOutAddressComponent } from './pages/checkOut/checkOutAddress/checkOutAddress.component';
import { checkOutDeliveryComponent } from './pages/checkOut/checkOutDelivery/checkOutDelivery.component';
import { checkOutPaymentComponent } from './pages/checkOut/checkOutPayment/checkOutPayment.component';
import { checkOutOrderReviewComponent } from './pages/checkOut/checkOutOrderReview/checkOutOrderReview.component';
import { checkOutConfirmComponent } from './pages/checkOut/checkOutConfirm/checkOutConfirm.component';
import { customerLoginComponent } from './pages/customerLogin/customerLogin.component';
import { allOrdersComponent } from './pages/orders/allOrders.component';
import { orderDetailsComponent } from './pages/orders/orderDetails/orderDetails.component';
import { profileComponent } from './pages/profile/profile.component';
import { addressesComponent} from './pages/addresses/addresses.component';
import { contactsComponent} from './pages/contacts/contacts.component';
import { changePasswordComponent} from './pages/profile/changePassWord/changePassWordComponent';
import { customerRegisterComponent } from './pages/customerRegister/customerRegister.component';
import { aboutComponent } from './pages/about/about.component';
import { corpGovernanceComponent } from './pages/corpGovernance/corpGovernance.component';
import { financeComponent } from './pages/finance/finance.component';
import { marineFleetComponent } from './pages/marineFleet/marineFleet.component';
import { operationComponent } from './pages/operation/operation.component';
import { marketingComponent } from './pages/marketing/marketing.component';
import { hrComponent } from './pages/hr/hr.component';
import { QAComponent } from './pages/QA/QA.component';
import { forgetPssComponent } from './pages/profile/forgetPass/forgetPss.component';
import { custActivationComponent } from './pages/customerActiviation/customerActiviation.component';
import { complaintComponent } from './pages/complaint/complaint.component'



export const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home',component:homeComponent},
  { path: 'categories',component:categoriesComponent},
  { path: 'categories/:id',component:liveStockComponent},
  { path: 'categories/detail/:id/:name',component:productDetailsComponent},
  { path: 'cart',component:shoppingCartComponent},
  { path: 'checkOut/checkOutAddress',component:checkOutAddressComponent},
  { path: 'checkOut/checkOutDelivery',component:checkOutDeliveryComponent},
  { path: 'checkOut/checkOutPayment',component:checkOutPaymentComponent},
  { path: 'checkOut/checkOutOrderReview',component:checkOutOrderReviewComponent},
  { path: 'checkOut/checkOutConfirm/:isSuccess',component:checkOutConfirmComponent},
  { path: 'login',component:customerLoginComponent},
  { path: 'orders',component:allOrdersComponent},
  { path: 'orders/detail/:id',component:orderDetailsComponent},
  { path: 'orders/detail/:item',component:orderDetailsComponent},
  { path: 'profile',component:profileComponent},
  { path: 'addresses',component:addressesComponent},
  { path: 'contact',component:contactsComponent},
  { path: 'changePassword',component:changePasswordComponent},
  { path: 'SignUp',component:customerRegisterComponent},
  { path: 'about',component:aboutComponent},
  { path: 'corpGovernance',component:corpGovernanceComponent},
  { path: 'finanace',component:financeComponent},
  { path: 'marineFleet',component:marineFleetComponent},
  { path: 'operation',component:operationComponent},
  { path: 'marketing',component:marketingComponent},
  { path: 'HR',component:hrComponent},
  { path: 'qualityAssuarance',component:QAComponent},
  { path: 'forgetPassword',component:forgetPssComponent},
  { path: 'complaint',component:complaintComponent},
  { path: 'customerActiviation',component:custActivationComponent}
]
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);