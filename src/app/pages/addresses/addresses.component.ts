import { Component, OnInit, ViewChild, ElementRef, NgModule, NgZone,ViewContainerRef, Inject } from '@angular/core';
import { FormsModule, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AgmCoreModule, MapsAPILoader, AgmMap } from '@agm/core';
import { } from '@types/googlemaps';
import * as $ from 'jquery';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { locationsService } from '../../services/location/locationService';
import { addressService } from '../../services/customer/addressService';
import { accountService } from '../../services/customer/customerAccount';
import { ordersService } from '../../services/customer/ordersService';
import { DaliogService } from '../../services/confirmMsg';
import { editAddressComponent } from '../addresses/editAddress/editAddressPopup';
import { addressesMapPopupComponent } from '../googleMapAddressPopup/googleMapAddressPopup'
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../services/shearedService';

declare var google: any;


@Component({
    moduleId: module.id,
    selector: 'app-addresses',
    templateUrl: 'addresses.component.html'
})

export class addressesComponent implements OnInit {

    constructor(private locationsS: locationsService, private addressS: addressService, private accountS: accountService, private translate: TranslateService,
        private customDialogService: DaliogService, private dailog: MatDialog,private ordersS: ordersService,private router: Router,
        private sheardS:shearedService) {
    }

    @ViewChild('agmMap')agmMap: AgmMap;

    public loading = false;
    locationPaciResult: any = {};
    locationMkaniResult: any = { governorate: {}, area: {}, block: {}, street: {} };
    locationPaciNum: number;
    locationMkaniNum: number;
    addressName: string = ''
    governorates: any = [];
    governorateId: string = '';
    areas: any = [];
    areaId: string = '';
    blocks: any = [];
    blockName: string = ''
    streets: any = [];
    streetId: string = '';
    addressObj: any = {};
    currentUser: any = {};
    userId: any = JSON.parse(localStorage.getItem('userId'))
    country = localStorage.getItem('country')
    mkani: any;
    addresses: any = [];
    addressDialog: MatDialogRef<editAddressComponent>
    addressGoogleDialog: MatDialogRef<addressesMapPopupComponent>
    countryCode = localStorage.getItem('countryCode');
    ordersCount: number;


    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
              if (res) {
                this.router.navigate(['/login'])
              }
            })
          }

        this.accountS.getCustomerById(this.userId).subscribe(result => {
            this.currentUser = result.data;
        }, error => console.log())

        this.getAddressesByUser();
        this.getOrderCount();
    }

    getAddressesByUser() {
        this.loading = true;
        this.addressS.getCustomerLocationsById(this.userId,this.country).subscribe(result => {
            this.addresses = result.data;
            this.loading = false;
        }, error => {
            this.loading = false;
            console.log('error')
        })

    }

    editAddress(addressId, currentUserId) {
        this.addressDialog = this.dailog.open(editAddressComponent, {
            width: '900px',
            height: '730px',
            data: { id: addressId, currentUserId: this.userId }

        })
        this.addressDialog.afterClosed().subscribe(() => this.getAddressesByUser())
    }

    addNewAddress() {
        this.addressDialog = this.dailog.open(editAddressComponent, {
            width: '900px',
            height: '730px',
            data: { id: 0, currentUserId: this.userId }

        })
        this.addressDialog.afterClosed().subscribe(() => this.getAddressesByUser())
    }

    getOrderCount(){
        this.ordersS.getOrderCount(this.countryCode,this.userId).subscribe(result =>{
            this.ordersCount = result.data
        },error => {
            console.log('error')
        })
    }

    logOut() {
        localStorage.removeItem('mwashi_currentAuthorzation');
        localStorage.removeItem('userName');
        localStorage.removeItem('userId')
        localStorage.removeItem('acceptMulti');
        localStorage.removeItem('cartItems');
        localStorage.removeItem('cartItemsId')
        localStorage.removeItem('cartQuantityCount');
        localStorage.removeItem('itemId');
        localStorage.removeItem('optionId');
        localStorage.removeItem('configId');
        localStorage.removeItem('resultConfigId');
        localStorage.removeItem('resultOption');
        localStorage.removeItem('order#');
        localStorage.removeItem('phone');
        localStorage.removeItem('registerUserName')
        localStorage.removeItem("customerActiviation")
        this.sheardS.cartCount = 0;
    
        this.router.navigate(['/categories'])
        location.reload();
      }
}


