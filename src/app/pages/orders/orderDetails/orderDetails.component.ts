import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DaliogService } from '../../../services/confirmMsg';
import { ordersService } from '../../../services/customer/ordersService';
import { cartService } from '../../../services/cart/cartService';
import { accountService } from '../../../services/customer/customerAccount'
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../../services/shearedService';


@Component({
    moduleId: module.id,
    selector: 'app-orderDetails',
    templateUrl: 'orderDetails.component.html'
})

export class orderDetailsComponent implements OnInit {

    constructor(private router: Router, private customDialogService: DaliogService,private ordersS: ordersService, private translate: TranslateService,
        private cartS: cartService,private route: ActivatedRoute,private accountS: accountService, private shearedS:shearedService) {
    }

    userId: any = JSON.parse(localStorage.getItem('userId'))
    countryCode = localStorage.getItem('countryCode');
    ordersCount: number;
    orderId: any = this.route.snapshot.params['id'];
    orderDetails:any= {};
    orderFeeds: any;
    orderSubtotal: any;
    loading: boolean = false;
    currentUser: any = []


    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
                if (res) {
                    this.router.navigate(['/login'])
                }
            })
        }
        this.getCurrentUser();
        this.getOrderCount();
        this.getOrderById();
        this.getOrderDeliveryFees();
    }

    getCurrentUser(){
        this.accountS.getUserById(this.userId).subscribe(result =>{
            this.currentUser = result.data;
        },error =>{
            if(error.status == 401){
            console.log('error')
            }
            console.log('error')
        })
    }

    getOrderCount(){
        this.ordersS.getOrderCount(this.countryCode,this.userId).subscribe(result =>{
            this.ordersCount = result.data;
        },error => {
            console.log('error')
        })
    }

    getOrderById(){
        this.loading = true;
        this.ordersS.getOrderById(this.countryCode,this.orderId).subscribe(result =>{
            this.orderDetails = result.data;
            this.loading = false;
        },error => {
            console.log('error')
        })
    }

    getOrderDeliveryFees() {
        this.ordersS.getOrderDeliveryFees(this.countryCode).subscribe(result => {
            this.orderFeeds = result.data;
        }, error => {
            console.log('error')
        })
    }

    logOut() {
        localStorage.removeItem('mwashi_currentAuthorzation');
        localStorage.removeItem('userName');
        localStorage.removeItem('userId')
        localStorage.removeItem('acceptMulti');
        localStorage.removeItem('cartItems');
        localStorage.removeItem('cartItemsId')
        localStorage.removeItem('cartQuantityCount');
        localStorage.removeItem('itemId');
        localStorage.removeItem('optionId');
        localStorage.removeItem('configId');
        localStorage.removeItem('resultConfigId');
        localStorage.removeItem('resultOption');
        localStorage.removeItem('order#');
        localStorage.removeItem('phone');
        localStorage.removeItem('registerUserName')
        localStorage.removeItem("customerActiviation")
        this.shearedS.cartCount = 0;
    
        this.router.navigate(['/categories'])
        location.reload();
      }
}


