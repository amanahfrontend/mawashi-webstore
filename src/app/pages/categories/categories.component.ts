import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { categoriesService } from '../../services/inventory/categoriesService'
import { cartService } from '../../services/cart/cartService'
import { Jsonp } from '@angular/http';
import { DaliogService } from '../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import * as $ from 'jquery';


@Component({
    moduleId: module.id,
    selector: 'app-categories',
    templateUrl: 'categories.component.html'
})

export class categoriesComponent implements OnInit {

    constructor(private categoriesS: categoriesService, private cartS: cartService, private router: Router, private customDialogService: DaliogService,
        private translate: TranslateService) {
    }

    public loading = false;
    categories: any = [];
    lang = localStorage.getItem('lang')
    cartStorage = JSON.parse(localStorage.getItem('cartItems'));
    optionsResult: any = [];
    disabledCategoty: boolean = false;
    countryCode = localStorage.getItem('countryCode');


    ngOnInit() {
        this.getAllCategories();
    }

    getAllCategories() {
        this.loading = true;
        this.categoriesS.getAllCategories(this.countryCode).subscribe(allCat => {
            this.categories = allCat.data;
            this.loading = false;
        }, error => {
            console.log('error')

        })
    }

    viewCategory(catId) {

        if (catId.config) {
            localStorage.setItem('resultConfigId', catId.config.id)
            localStorage.setItem('resultOption', JSON.stringify(catId.config.options))
            localStorage.setItem('acceptMulti', catId.config.acceptMulti)
        }
        if (this.cartStorage) {
            if (this.cartStorage.length > 0) {
                for (let i = 0; i < this.cartStorage.length; i++) {
                    let acceptMulti = JSON.parse(localStorage.getItem('acceptMulti'))

                    if (this.cartStorage[i].item.fK_Category_Id == catId.id) {
                        this.router.navigate(['categories/' + catId.id]);
                    }
                    else if (this.cartStorage[i].acceptMulti == localStorage.getItem('acceptMulti') == true) {
                        this.router.navigate(['categories/' + catId.id]);
                    }
                    else if ((this.cartStorage[i].acceptMulti === acceptMulti) === false) {
                        this.loading = false;
                        this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                        break;
                    }
                    else if (this.cartStorage[i].acceptMulti != acceptMulti) {
                        this.loading = false;
                        this.customDialogService.alert(this.translate.instant('note'), this.translate.instant('cntAddItemCat'), false);
                        break;
                    }

                }
            }
            else {
                this.router.navigate(['/categories/', catId.id])
            }
        }

        else {
            this.router.navigate(['/categories/', catId.id]);
        }
    }

}


