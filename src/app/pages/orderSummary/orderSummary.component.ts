import { Component, OnInit, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { cartService } from '../../services/cart/cartService'
import { ordersService } from '../../services/customer/ordersService'


@Component({
    moduleId: module.id,
    selector: 'app-orderSummary',
    templateUrl: 'orderSummary.component.html'
})

export class orderSummaryComponent implements OnInit {

    constructor(private router: Router, private cartS: cartService, private ordersS: ordersService) {

    }

    isFrame: boolean = true;
    cartItemsId = JSON.parse(localStorage.getItem('cartItemsId'))
    submitedCartObj: any = {};
    userId: any = JSON.parse(localStorage.getItem('userId'))
    orderSubtotal: number;
    tax: number;
    total: number;
    orderFeeds: any ;
    countryCode = localStorage.getItem('countryCode');

    ngOnInit() {
        this.getOrderDeliveryFees();
        this.getCurrentCart();
    }

    getOrderDeliveryFees() {
        this.ordersS.getOrderDeliveryFees(this.countryCode).subscribe(result => {
            this.orderFeeds = result.data;
        }, error => {
            console.log('error')
        })
    }

    getCurrentCart() {
        this.cartS.getCartOverview(this.countryCode,this.userId).subscribe(result => {
            this.submitedCartObj = result.data;
            this.tax = Math.ceil(this.submitedCartObj.vat);
            this.orderSubtotal = Math.ceil(this.submitedCartObj.price);
            this.ordersS.getOrderDeliveryFees(this.countryCode).subscribe(result => {
                this.orderFeeds = result.data;
            this.total = Math.ceil(this.submitedCartObj.price + this.tax + this.orderFeeds);
        }, error => {
            console.log('error')
        })
        }, error => console.log('error'))
    }


}


