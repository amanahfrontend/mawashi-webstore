import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';


import { Observer } from "rxjs/Observer";


const CART_KEY = "cart";
@Injectable()

export class categoriesService {

    constructor(private http:Http , private config: webService){
    }

    getAllCategories(countryCode):Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesInventory.replace('{0}',countryCode) + this.config.getAllCategories
        return this.http.get(url)
            .map(response => response.json())
    }
    getCategorieById(countryCode,catId):Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesInventory.replace('{0}',countryCode) + this.config.getCategorieById.replace('{1}',catId)
        return this.http.get(url)
            .map(response => response.json())
    }
    getProductById(countryCode,itemId):Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesInventory.replace('{0}',countryCode) + this.config.getItemDetailsById.replace('{1}',itemId)
        return this.http.get(url)
            .map(response => response.json())
    }
    GetDeliveryOptionsByCategoryConfigId(countryCode,configOptionId):Observable<any>{
      const url = this.config.baseIp + this.config.baseServicesInventory.replace('{0}',countryCode) + this.config.GetDeliveryOptionsByCategoryConfigIdUrl.replace('{1}',configOptionId)
      return this.http.get(url, {headers: this.config.headers})
          .map(response => response.json())
  }


}