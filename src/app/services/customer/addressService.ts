import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class addressService {
    constructor(private http: Http, private config: webService) {
    }

    postAddress(obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.postAdressUrl
        return this.http.post(url, obj, {headers:this.config.headers})
            .map(response => response.json())
    }
    editAddress(obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.editAddressUrl
        return this.http.put(url, obj,{headers:this.config.headers})
            .map(response => response.json())
    }
    getCustomerLocationsById(custId,countryId): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.getCustomerLocationByIdUrl.replace('{0}', custId).replace('{1}', countryId)
        return this.http.get(url, {headers:this.config.headers})
            .map(response => response.json())
    }
    getAddressById(addressId):Observable<any>{
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.getAddressById.replace('{0}', addressId)
        return this.http.get(url, {headers:this.config.headers})
            .map(response => response.json())
    }
    getAllCountries(): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesCustomer + this.config.getAllCountriesUrl
        return this.http.get(url, {headers:this.config.headers})
            .map(response => response.json())
    }
}