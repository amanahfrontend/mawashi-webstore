import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { accountService } from '../../services/customer/customerAccount'
import { DaliogService } from '../../services/confirmMsg';
import { Location } from '@angular/common';
import { cartService } from '../../services/cart/cartService';
import { TranslateService } from '@ngx-translate/core';
import 'rxjs/add/operator/filter';
import { shearedService } from '../../services/shearedService';




@Component({
    moduleId: module.id,
    selector: 'app-customerLogin',
    templateUrl: 'customerLogin.component.html'
})

export class customerLoginComponent implements OnInit {

    constructor(private accountS: accountService, private customDialogService: DaliogService, private cartS: cartService, private router: Router,
        private Location: Location, private translate: TranslateService, private rout: ActivatedRoute,  private sheardS: shearedService) {

        
    }

    loading: boolean = false;
    loginObj: any = {};
    cartObj: any = [];
    cartItemsId: any;
    cartQuantityCount: any;
    itemObj: any = {
        Fk_Item_Id: 0,
        FK_Quantity_Id: 0,
        FK_Cutting_Id: 0,
        FK_CategoryConfig_Id: 0,
        FK_Option_Id: 0
    }
    previousUrl: any;


    ngOnInit() {

    }

    initializeCart(countryCode, userId, token) {
        if (localStorage.getItem("cartItems") != null) {
            this.cartObj = JSON.parse(localStorage.getItem("cartItems"));
            localStorage.getItem("cartQuantityCount")
        }
        this.cartS.getCurrentCart(countryCode, userId, token).subscribe(result => {
            if (result.data) {
                if (this.cartObj.length == 0) {
                    result.data.cartItems.forEach(item => {
                        // this.itemObj.FK_CategoryConfig_Id = item.fK_CategoryConfig_Id;
                        // this.itemObj.FK_Option_Id = item.fK_Option_Id;
                        // this.itemObj.FK_Cutting_Id = item.fK_Cutting_Id;
                        // this.itemObj.FK_Quantity_Id = item.fK_Quantity_Id;
                        // this.itemObj.Fk_Item_Id = item.fk_Item_Id;
                        this.cartObj.push(item);
                    });
                }
                localStorage.setItem("cartItems", JSON.stringify(this.cartObj));
                localStorage.setItem("cartItemsId", result.data.id);
                localStorage.setItem("cartQuantityCount", result.data.cartItems[0].quantity.value);
                this.sheardS.cartCount = localStorage.getItem("cartQuantityCount");
               
            } else {
                localStorage.setItem("cartItemsId", '0');
            }
            location.reload();

        }, error => {
            console.log("Error");
        })
    }

    loginUser() {
        this.loading = true;
        this.previousUrl= localStorage.getItem('previousUrl')
        this.accountS.login(this.loginObj.userName, this.loginObj.password).subscribe(result => {
            this.loading = false;
            if(this.previousUrl == 'customerActiviation'){
                this.router.navigate(['/home'])
            }
            else{
                this.Location.back();
            }

            this.initializeCart(localStorage.getItem('countryCode'), result.userId, result.token);
            localStorage.removeItem('previousUrl')
            this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('lognSucss'), false);

        }, error => {
            this.loading = false;
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('somThingWrong'), false);
        })
    }
}


