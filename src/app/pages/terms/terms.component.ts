import { Component, OnInit, Input,HostListener } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DaliogService } from '../../services/confirmMsg';
import { checkOutService } from '../../services/checkout/checkoutService'
import { SafePipe } from '../../pipes/safePipe';
import { DomSanitizer } from '@angular/platform-browser';
import { ordersService } from '../../services/customer/ordersService'
import { TranslateService } from '@ngx-translate/core';



@Component({
    moduleId: module.id,
    selector: 'app-terms',
    templateUrl: 'terms.component.html'
})

export class termsComponent implements OnInit {

    constructor(public dialogRef: MatDialogRef<termsComponent>, private customDialogService: DaliogService, private router: Router,
        private checkOutS: checkOutService,private sanitizer: DomSanitizer,private orderS:ordersService,private translate: TranslateService
        ) {

    }

    countryCode = localStorage.getItem('countryCode');
    htmlToAdd: any = "";
    checkoutObj: any = {};
    customerId: any = JSON.parse(localStorage.getItem('userId'))
    locationId = JSON.parse(localStorage.getItem('locationId'))
    deliveryDate = localStorage.getItem('deliverDate')
    methodId = localStorage.getItem('PaymentTypeCode')
    deliveryNote = localStorage.getItem('deliveryNote')
    deliverTypeId = localStorage.getItem('deliveryTypeId')
    promoCode = localStorage.getItem('promoCode')
    payObj: string = ''
    urlObj: any;
    isPOst: boolean = false;
    isFrame: boolean = true;
    cartItemsId = JSON.parse(localStorage.getItem('cartItemsId'))
    submitedCartObj: any = {};
    userId: any = JSON.parse(localStorage.getItem('userId'))
    total: number;
    submitedOrder: any = {}


    ngOnInit() {
        this.checkOutS.getTerms(this.countryCode).subscribe(html => {
            this.htmlToAdd = html;

        }, error => {
            console.log(error)
        })

    }

    
    postOrder() {
        this.dialogRef.close();
    }
   

}


