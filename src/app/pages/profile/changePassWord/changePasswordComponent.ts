import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { accountService } from '../../../services/customer/customerAccount';
import { ordersService } from '../../../services/customer/ordersService';
import { DaliogService } from '../../../services/confirmMsg';
import { TranslateService } from '@ngx-translate/core';
import { shearedService } from '../../../services/shearedService';


@Component({
    moduleId: module.id,
    selector: 'app-changePasswordComponent',
    templateUrl: 'changePasswordComponent.html'
})

export class changePasswordComponent implements OnInit {

    constructor(private accountS:accountService, private customDialogService: DaliogService,private router: Router,
        private ordersS:ordersService, private translate: TranslateService, private sheardS:shearedService) {
    }

    public loading = false;
    userName: any = JSON.parse(localStorage.getItem('userName'))
    passObj: any = {};
    ordersCount: number;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    countryCode = localStorage.getItem('countryCode');
    currentUser: any = {};

    
    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.confirm('Login', 'Please Login First', true);
            result.subscribe((res) => {
              if (res) {
                this.router.navigate(['/login'])
              }
            })
          }
          this.getOrderCount();
          this.getCurrentUser();
    }

    getCurrentUser(){
        this.loading = true;
        this.accountS.getUserById(this.userId).subscribe(result =>{
            this.currentUser = result.data;
            this.loading = false;
        },error =>{
            this.loading = false;
            console.log('error')
        })
    }
    changePass(){
        this.passObj.userName = this.userName;
        this.accountS.changePassword(this.passObj).subscribe(result =>{
            if(result.data == true){
                this.customDialogService.alert(this.translate.instant('done'), this.translate.instant('chngPssucss'), false);
                this.accountS.login(this.userName,this.passObj.newPassword).subscribe(loginResult =>{
                    this.router.navigate(['/profile'])
                },error =>{
                    console.log('error')
                })
            }
            else{
                this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('chngPassWrong'), false);
                this.passObj = {};
            }
        }, error => {
            this.customDialogService.alert(this.translate.instant('fail'), this.translate.instant('chngPassWrong'), false);
            this.passObj = {};

        })
    }
    
    getOrderCount(){
        this.ordersS.getOrderCount(this.countryCode,this.userId).subscribe(result =>{
            this.ordersCount = result.data
        },error => {
            console.log('error')
        })
    }

    logOut() {
        localStorage.removeItem('mwashi_currentAuthorzation');
        localStorage.removeItem('userName');
        localStorage.removeItem('userId')
        localStorage.removeItem('acceptMulti');
        localStorage.removeItem('cartItems');
        localStorage.removeItem('cartItemsId')
        localStorage.removeItem('cartQuantityCount');
        localStorage.removeItem('itemId');
        localStorage.removeItem('optionId');
        localStorage.removeItem('configId');
        localStorage.removeItem('resultConfigId');
        localStorage.removeItem('resultOption');
        localStorage.removeItem('order#');
        localStorage.removeItem('phone');
        localStorage.removeItem('registerUserName')
        localStorage.removeItem("customerActiviation")
        this.sheardS.cartCount = 0;
    
        this.router.navigate(['/categories'])
        location.reload();
      }

}


