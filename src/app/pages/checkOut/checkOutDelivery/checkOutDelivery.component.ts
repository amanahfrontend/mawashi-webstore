import { Component, OnInit, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery'
import { DaliogService } from '../../../services/confirmMsg';
import { cartService } from '../../../services/cart/cartService';
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { checkOutAddressComponent } from '../checkOutAddress/checkOutAddress.component';
import { shearedService } from '../../../services/shearedService';


@Component({
    moduleId: module.id,
    selector: 'app-checkOutDelivery',
    templateUrl: 'checkOutDelivery.component.html'
})

export class checkOutDeliveryComponent implements OnInit {

    constructor(private router: Router, private customDialogService: DaliogService, private cartS: cartService, private translate: TranslateService,
        private Location: Location, private sharedS: shearedService) {
    }

    public loading = false;
    deliverDate: Date;
    deliveryNote: string = ''
    promoCode: string = '';
    promoCodeObj: any = {};
    promoCodeResult: any;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    countryCode = localStorage.getItem('countryCode')
    cartResult: any = {};
    cartResultLength: any;
    lang = localStorage.getItem('lang');

    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
                if (res) {
                    this.router.navigate(['/login'])
                }
            })
        }

        $('body').on('keydown', 'input', function (e) {
            if (e.which === 32 && e.target.selectionStart === 0) {
                return false;
            }
        });
        $('body').on('keydown', 'textarea', function (e) {
            if (e.which === 32 && e.target.selectionStart === 0) {
                return false;
            }
        });
        this.getCurrentCart();
    }

    getCurrentCart() {
        this.cartS.getCartOverview(this.countryCode, this.userId).subscribe(result => {
            this.cartResult = result.data;
            this.cartResultLength = this.cartResult.cartItems.length;
        }, error => {
            console.log("Error");
        })
    }

    checkPromoCodeValid() {
        this.loading = true;
        this.promoCodeObj.CustomerId = this.userId;
        this.promoCodeObj.PromoCode = this.promoCode;
        this.cartS.checkPromoCodeValid(this.countryCode, this.promoCodeObj).subscribe(result => {
            this.promoCodeResult = result.data;
            this.loading = false;
            if (this.promoCodeResult == true) {
                this.customDialogService.confirm(this.translate.instant('done'), this.translate.instant('promoSucss'), false);
            }
            else {
                this.customDialogService.confirm(this.translate.instant('invld'), this.translate.instant('promoFail'), false);
            }
        }, error => {
            this.customDialogService.confirm(this.translate.instant('fail'), this.translate.instant('somThingWrong'), false);
        })
    }

    back() {
        this.sharedS.deliveryTypeId;
        this.sharedS.locationId;
        this.sharedS.deliveryDate;
        this.router.navigate(['checkOut/checkOutAddress'])
    }

    submitDeliverDate() {
        if (this.promoCodeResult == true) {
            // localStorage.setItem('promoCode', this.promoCode)
            this.sharedS.promoCode = this.promoCodeResult
        }
        // localStorage.setItem('deliveryNote', this.deliveryNote)
        this.sharedS.deliveryNote;
        this.router.navigate(['/checkOut/checkOutPayment'])
    }

    navigateBack() {
        this.Location.back();
    }

}


