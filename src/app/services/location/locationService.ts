import { Injectable } from '@angular/core';
import { Http, Headers} from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class locationsService {
    constructor(private http:Http , private config: webService){
    }

    ///////////////Makani//////////
    getMakaniInfoFromCoord(lat,log):Observable<any>{
        const url = this.config.baseIp + this.config.baseServiceLocation + this.config.getMakaniInfoFromCoord.replace('{0}',lat)
            .replace('{1}',log)
        return this.http.get(url)
            .map(response => response.json())
    }
    getMakaniInfoFromNumber(makaniNum):Observable<any>{
        const url = this.config.baseIp + this.config.baseServiceLocation + this.config.getMakaniInfoFromNumber
            .replace('{0}',makaniNum)
        return this.http.get(url)
            .map(response => response.json())
    }

    /////////Paci////////////////
    getPaciByNum(paciNum):Observable<any>{
        const url = this.config.baseIp + this.config.baseServiceLocation + this.config.getLocationByPaci.replace('{0}',paciNum)
        return this.http.get(url)
            .map(response => response.json())
    }
    getAllGovernorates():Observable<any>{
        const url = this.config.baseIp + this.config.baseServiceLocation + this.config.getAllGovernorates
        return this.http.get(url)
            .map(response => response.json())
    }
    getAllAreas(govId):Observable<any>{
        const url = this.config.baseIp + this.config.baseServiceLocation + this.config.getAreas.replace('{0}', govId)
        return this.http.get(url)
            .map(response => response.json())
    }
    getAllBlocks(areaId):Observable<any>{
        const url = this.config.baseIp + this.config.baseServiceLocation + this.config.getBlocks.replace('{0}', areaId)
        return this.http.get(url)
            .map(response => response.json())
    }
    getAllStreets(govId,areaId,blockId):Observable<any>{
        const url = this.config.baseIp + this.config.baseServiceLocation + this.config.getStreets.replace('{0}', govId).replace('{1}', areaId)
        .replace('{2}',blockId) 
               return this.http.get(url)
            .map(response => response.json())
    }
}