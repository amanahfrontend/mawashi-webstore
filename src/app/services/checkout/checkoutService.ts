import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import 'rxjs/add/operator/map';
import { webService } from '../webService';

@Injectable()

export class checkOutService {
    constructor(private http: Http, private config: webService) {
    }

    postCheckOut(countryCode,obj): Observable<any> {
        const url = this.config.baseIp + this.config.baseServicesOrders.replace('{0}',countryCode) + this.config.postOrderCheckoutUrl
        return this.http.post(url, obj, {headers:this.config.checkoutHeader})
            .map(response => response.json())
    }
    getTerms(countryCode): Observable<any> {
        const url = this.config.baseIp + this.config.baseContentapi.replace('{0}',countryCode) + this.config.getTermsTemplateUrl
        return this.http.get(url)
            .map(response => response.text())
    }
    getAds(countryCode): Observable<any> {
        const url = this.config.baseIp + this.config.baseContentAdsapi + this.config.getAllAdsUrl.replace('{0}',countryCode)
        return this.http.get(url)
            .map(response => response.json())
    }
}