import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { paymentService } from '../../../services/payment/paymentService'
import { DaliogService } from '../../../services/confirmMsg';
import { count } from 'rxjs/operators';
import { cartService } from '../../../services/cart/cartService'
import { TranslateService } from '@ngx-translate/core';
import { Location } from '@angular/common';
import { shearedService } from '../../../services/shearedService';



@Component({
    moduleId: module.id,
    selector: 'app-checkOutPayment',
    templateUrl: 'checkOutPayment.component.html'
})

export class checkOutPaymentComponent implements OnInit {

    constructor(private paymentS: paymentService, private router: Router,private customDialogService: DaliogService,
        private cartS: cartService, private translate: TranslateService,private Location: Location,private sheardS:shearedService) {
    }

    public loading = false;
    userId: any = JSON.parse(localStorage.getItem('userId'))
    medthods: any = {};
    PaymentTypeCode: any;
    country = localStorage.getItem('country')
    countryCode = localStorage.getItem('countryCode')
    deliveryType = localStorage.getItem('deliveryTypeId')
    cartResult: any = {};
    cartResultLength: any;
    lang = localStorage.getItem('lang');

    ngOnInit() {
        if (!localStorage.getItem('mwashi_currentAuthorzation')) {
            this.loading = false;
            let result = this.customDialogService.alert(this.translate.instant('login'), this.translate.instant('loginFirst'), true);
            result.subscribe((res) => {
              if (res) {
                this.router.navigate(['/login'])
              }
            })
          }

        this.getPaymentMethod();
    }

    getPaymentMethod() {
        this.paymentS.getPaymentMethods(this.countryCode,this.country,this.deliveryType).subscribe(result => {
            this.medthods = result;
        }, error => {
            console.error();
        })
    }

    back(){
        this.sheardS.promoCode;
        this.sheardS.deliveryNote;
        this.router.navigate(['/checkOut/checkOutDelivery'])
    }

    submitPayMethod() {
        // localStorage.setItem('PaymentTypeCode', this.PaymentTypeCode)
        this.sheardS.PaymentTypeCode;
        this.router.navigate(['/checkOut/checkOutOrderReview'])
    }
    
    getCurrentCart() {
        this.cartS.getCartOverview(this.countryCode, this.userId).subscribe(result => {
            this.cartResult = result.data;
            this.cartResultLength = this.cartResult.cartItems.length;
        }, error => {
            console.log("Error");
        })
    }

    navigateBack() {
        this.Location.back();
    }
}


